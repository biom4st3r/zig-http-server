# HTTP server written in Zig
* Default behavior is a file server from the current directory on 0.0.0.0:8080
* A fast cross-plateform and lightweight http server with configurable endpoints. Built for extensibility
* Can serve 60_000 requests per second @ 4 Threads with no payload or 50_000 with a real webpage
* Support for 206 Partial Content
* Support for Chunk Transfer

## Builtin Endpoints
### File Server
* Serves files from the provided root directory or the cwd

### Upload
* Uploads provided file to the directory you are currently viewing

### WebSocket
* Manages creation, Framing and un-Framing messages across a WebSocket

### NestedEP
* Allow you to register an addition layer of endpoints. Useful for have authentiated and unauth areas.

### WrappedEP
* Wraps another endpoint to add more conditions. Pairs well with NestedEP