#!/bin/python
import socket
import sys
import time
import random

def send_sizes(host, port, request):
    # Create a socket object
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to the server
        client_socket.connect((host, port))

        # Send the HTTP request in randomly sized chunks between 1 and 17 bytes
        request_bytes = request.encode('utf-8')
        sent_bytes = 0

        while sent_bytes < len(request_bytes):
            chunk_size = random.randint(1, 17)
            chunk = request_bytes[sent_bytes:sent_bytes + chunk_size]
            sent_bytes += len(chunk)
            client_socket.send(chunk)
            print(chunk.decode(), end="", flush=True)
            time.sleep(0.1)

        # Receive and print the response
        response = b""
        while True:
            data = client_socket.recv(1024)
            if not data:
                break
            response += data

        print(response.decode())
    
    except Exception as e:
        print(f"Error: {e}")
    
    finally:
        # Close the socket
        client_socket.close()


def send_loris(host, port, request):
    # Create a socket object
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to the server
        client_socket.connect((host, port))

        # Send the HTTP request
        for x in request.encode():
            time.sleep(0.1)
            client_socket.send(chr(x).encode('utf-8'))
            
            print(chr(x),end="", flush=True)
            
        # client_socket.send(request.encode())

        # Receive and print the response
        response = b""
        while True:
            data = client_socket.recv(4096)
            if not data:
                break
            response += data

        print(response.decode())
    
    except Exception as e:
        print(f"Error: {e}")
    
    finally:
        # Close the socket
        client_socket.close()
    

def send_request(host, port, request):
    # Create a socket object
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to the server
        client_socket.connect((host, port))

        # Send the HTTP request
        client_socket.send(request.encode())

        # Receive and print the response
        response = b""
        while True:
            data = client_socket.recv(1024)
            if not data:
                break
            response += data

        print(response.decode())
    
    except Exception as e:
        print(f"Error: {e}")
    
    finally:
        # Close the socket
        client_socket.close()

if __name__ == "__main__":
    # Define the server's address and port
    if len(sys.argv) != 3:
        print("Usage: python http_client.py <host> <port>")
        sys.exit(1)

    server_host = sys.argv[1]
    server_port = int(sys.argv[2])


    # Define the HTTP request
    # http_request = (
    #     "GET / HTTP/1.1\r\n"
    #     "Host: example.com\r\n"
    #     "Connection: close\r\n"
    #     "\r\n"
    # )
    http_request = (
        "GET / HTTP/1.1\r\n"
        "Host: asdf.com\r\n"
        "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:108.0) Gecko/20100101 Firefox/108.0\r\n"
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8\r\n"
        "Accept-Language: en-US,en;q=0.5\r\n"
        "Accept-Encoding: gzip, deflate\r\n"
        "Connection: keep-alive\r\n"
        "Upgrade-Insecure-Requests: 1\r\n"
        "Pragma: no-cache\r\n"
        "Cache-Control: no-cache\r\n"
        "\r\n"
    )

    # Send the HTTP request and receive the response
    # send_request(server_host, server_port, http_request)
    send_request(server_host, server_port, http_request)
