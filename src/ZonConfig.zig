pub const std = @import("std");

root_path: []const u8 = ".",
listening_ip: []const u8 = "0.0.0.0",
port: u16 = 8080,
threads: usize = @min(4, @as(u64, @intCast(std.Thread.getCpuCount() catch unreachable))),
gen_port: bool = false,
reuse_port: bool = false,
logging_level: u8 = 0,
log_file: ?[]const u8 = null,
endpoints: [][]const u8 = .{},

pub fn loadOrCreate(path: []const u8, alloc: std.mem.Allocator) !void {
    var content = try std.fs.cwd().readFileAlloc(alloc, path, 2 * 1024 * 1024);
    std.zig.Ast.parse(alloc, content, .zon);
}
