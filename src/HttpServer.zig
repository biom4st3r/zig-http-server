pub const Endpoints = @import("endpoints.zig");
pub const DataTypes = @import("data_types.zig");
pub const HtmlMinifier = @import("comptime_shrink.zig").shrink_embed;
pub const Request = DataTypes.Request;
pub const Response = DataTypes.ResponseBuilder;
pub const Strings = DataTypes.Strings;
pub const Args = @import("args.zig");
pub const HTTP_CONFIG = Args.HTTP_CONFIG;

const std = @import("std");
const StreamServer = std.net.Server;

const eql = std.mem.eql;
const print = std.debug.print;

const FileRecord = DataTypes.FileRecord;
const Self = @This();

pub fn log_print(lvl: @TypeOf(LOGGING), comptime fmt: []const u8, args: anytype) void {
    if (@intFromEnum(LOGGING) >= @intFromEnum(lvl)) {
        if (ERROR_LOG_FILE) |f| {
            f.writer().print("{s}[{d}]", .{ @tagName(lvl), std.time.timestamp() }) catch {};
            f.writer().print(fmt, args) catch {};
        } else {
            print("{s}[{d}]", .{ @tagName(lvl), std.time.timestamp() });
            print(fmt, args);
        }
    }
}

pub const HTTP11_TEMPLATE = Response
    .add_code("HTTP/1.1", "{s}")
    .add_field("Server", "zhttp-server");

pub const TEMPLATE_RESPONSE = HTTP11_TEMPLATE
    .content_type("{s}")
    .content_length("{d}")
    .connection(.@"keep-alive")
    .add_field("Accept-Ranges", "bytes")
    .end();

const TEMPLATE_CHUNKED_RESPONSE = HTTP11_TEMPLATE
    .content_type("{s}")
    .content_length("{d}")
    .connection(.@"keep-alive")
    .add_field("Transfer-Encoding", "chunked")
    .end();

pub fn create_simple_response(buffer: []u8, status: []const u8, content_type: []const u8, content: []const u8) !?[]const u8 {
    if (TEMPLATE_RESPONSE.len - 9 + status.len + 3 + content.len + content_type.len > buffer.len) return null;
    const header = try Strings.format(buffer, TEMPLATE_RESPONSE, .{ status, content_type, content.len });
    const body = try Strings.format(buffer[header.len..], "{s}", .{content});
    return buffer[0 .. header.len + body.len];
}

pub const hexchars = "0123456789ABCDEF";

pub fn send_chunked_response(client: *StreamServer.Connection, content_length: usize, buffer: []u8, status: []const u8, content_type: []const u8, reader: anytype, read: fn (@TypeOf(reader), []u8) anyerror!usize) !void {
    // TODO Send to thread queue so it doesn't consume a whole thread
    // format and send the http header
    const header = try Strings.format(buffer, TEMPLATE_CHUNKED_RESPONSE, .{
        status,
        content_type,
        content_length,
    });
    _ = try client.stream.write(header);
    // Reserve room for the size octet and CRNL
    var chunk_header = buffer[0..10];
    chunk_header[8] = '\r';
    chunk_header[9] = '\n';
    const chunk = buffer[10 .. buffer.len - 2];
    var amount_read: usize = try @call(.auto, read, .{ reader, chunk });
    while (true) : (amount_read = try @call(.auto, read, .{ reader, chunk })) {
        const last = amount_read < chunk.len;
        var index: usize = chunk_header.len - 1 - 2;
        // Convert to hex
        var base10 = amount_read;
        while (base10 > 0) : (index -= 1) {
            chunk_header[index] = hexchars[base10 % 16];
            base10 = @divTrunc(base10, 16);
        }
        index += 1; // TODO the loop should handle this
        const pkt_start = index;
        const end = index + (10 - index) + amount_read + 2;
        buffer[end - 2] = '\r';
        buffer[end - 1] = '\n';
        // TODO std.os.sendfile
        _ = try client.stream.write(buffer[pkt_start..end]);
        if (last) break;
    }
    _ = try client.stream.write("0\r\n\r\n");
}

const PARTIAL_HEADER = Response
    .add_code("HTTP/1.1", "206 Partial Content")
    .add_field("Server", "zhttp-server")
    .content_type("{s}") //application/octet-stream
    .content_length("{d}")
    .connection(.@"keep-alive")
    .add_field("Accept-Ranges", "bytes")
    .add_field("Content-Range", "bytes {d}-{d}/{d}")
    .end();

const RangeNotSatisfiable = Response
    .add_code("HTTP/1.1", "416 Requested Range Not Satisfiable")
    .add_field("Server", "zhttp-server")
    .connection(.@"keep-alive")
    .add_field("Content-Range", "bytes */{s}")
    .end();

pub fn serve_partial_response(client: StreamServer.Connection, request: *Request, file: std.fs.File, buffer: []u8, approx_chunk_size: usize, content_type: []const u8) !bool {
    if (request.get_browser().matches_browser(.{ .custom = "VLC" })) {
        if (request.query) |q| if (Strings.find("vlc=false", q)) |_| {} else {
            return try vlc_serve_partial_response(client, request, file, buffer, content_type);
        };
        // Works better than standard, but not perfect
    }

    const TO_SEND = @max(1, @divFloor(approx_chunk_size, buffer.len)); // control chunk size as multiples of buffer size
    const range = request.get_range_parsed() catch |err| switch (err) {
        error.NotSupported => {
            return false;
        },
        else => {
            return err;
        },
    } orelse return false;

    // where requested to start read
    const stats = try file.stat();
    // amount to be read based on buffer size and length of file
    const to_read = @min(buffer.len * TO_SEND, stats.size - range.start);
    const end = range.start + to_read - 1; // range is inclusive
    // formatted Partial header
    const written = try std.fmt.bufPrint(buffer, PARTIAL_HEADER, .{ content_type, to_read, range.start, end, stats.size });
    // send header
    const sendfile = true;
    if (sendfile) {
        _ = try std.posix.sendfile(client.stream.handle, file.handle, range.start, to_read, &.{.{ .iov_base = written.ptr, .iov_len = written.len }}, &.{}, 0);
    } else {
        _ = try client.stream.write(written);
        // send promised data
        try file.seekTo(range.start);
        for (0..TO_SEND) |_| {
            const chunk = buffer[0..try file.readAll(buffer)];
            _ = try client.stream.write(chunk);
            if (chunk.len < buffer.len) break;
        }
    }

    return true;
}

pub fn vlc_serve_partial_response(client: StreamServer.Connection, request: *Request, file: std.fs.File, buffer: []u8, content_type: []const u8) !bool {
    const range = request.get_range_parsed() catch |err| switch (err) {
        error.NotSupported => {
            return false;
        },
        else => {
            return err;
        },
    } orelse return false;

    const stats = try file.stat();

    // amount to be read based on buffer size and length of file
    const to_read = if (range.end) |e| (e - range.start) else (stats.size - range.start);
    // formatted Partial header
    const written = try std.fmt.bufPrint(buffer, PARTIAL_HEADER, .{
        if (content_type.len != 0) content_type else "application/octet-stream",
        to_read,
        range.start,
        (if (range.end) |e| e else stats.size) - 1,
        stats.size,
    });
    // send header
    _ = try client.stream.write(written);
    // send promised data
    try file.seekTo(range.start);
    var i: usize = 0;
    while (i < to_read) {
        const chunk = buffer[0..@min(buffer.len, to_read - i)];
        _ = try file.readAll(chunk);
        // var chunk = buffer[0..try file.readAll(buffer)];
        i += chunk.len;
        _ = try client.stream.write(chunk);
        // if (chunk.len < buffer.len) break;
    }

    return true;
}

const SIMPLE_RESPONSE = HTTP11_TEMPLATE
    .content_type("{s}")
    .content_length("{d}")
    .connection(.@"keep-alive")
    .add_field("Accept-Ranges", "bytes")
    .end();

const TEMPLATE_REDIRECT_RESPONSE = Response
    .add_code("HTTP/1.1", "301 Moved Permanently")
    .add_field("Server", "zhttp-server")
    .content_type("text/plain")
    .location("{s}")
    .connection(.@"keep-alive")
    .add_field("Accept-Ranges", "bytes")
    .end();

pub fn send_redirect_response(client: *StreamServer.Connection, destination: []const u8) !void {
    _ = try client.stream.writer().print(TEMPLATE_REDIRECT_RESPONSE, .{destination});
}

pub const MiddlewareBox = struct {
    pub const Stage = enum {
        // REQUESTS
        ///Returning true from Pre_request is a signal to close the connection
        Pre_request, // before initial request is parsed
        //Returning true from Post_request is a signal to close the connection
        Post_request, // after initial request is parsed, before endpoint determined
        // RESPONSES
        //Returning true from Pre_Headers is a signal that your middleware has
        Pre_Headers, // Before headers are sent
        Post_Headers, // After headers sent, before \r\n
    };
    pub const StageResponse = enum {
        /// Act as if middleware did nothing
        Pass,
        /// middleware requests the connection be closed ASAP
        Cancel,
        /// middleware has handled the current stage of the connection, caller should do nothing else to it.
        /// middleware is expected to resubmit the connection for keep-alive if applicable.
        Handled,
    };

    pub const StageData = union(Stage) {
        Pre_request: struct { server: *Self, client: StreamServer.Connection },
        Post_request: *IEndpoint.Ctx,
        Pre_Headers: struct { ctx: *IEndpoint.Ctx, code: []const u8 },
        Post_Headers: struct { ctx: *IEndpoint.Ctx, code: []const u8, headers: []const u8 },
    };

    pub fn get_wares(self: *@This(), stage: Stage) *std.ArrayList(T) {
        return &self.middleware[@intFromEnum(stage)];
    }

    pub fn run_stage(self: *@This(), stage: Stage, data: *StageData) StageResponse {
        const wares = self.get_wares(stage);
        for (wares.items) |ware| {
            const resp = ware.intercept(stage, data);
            if (resp != .Pass) return resp;
        }
        return .Pass;
    }

    pub const Middleware = struct {
        interceptFn: *const @TypeOf(intercept),
        pub fn intercept(self: *Middleware, stage: Stage, data: *StageData) StageResponse {
            return @call(.auto, self.interceptFn, .{ self, stage, data });
        }
    };
    pub var singleton: ?MiddlewareBox = null;
    const T = *Middleware;
    middleware: [std.enums.values(Stage).len]std.ArrayList(T),
    pub fn init(self: *@This(), alloc: std.mem.Allocator) void {
        for (&self.middleware) |*list| {
            list.* = std.ArrayList(T).init(alloc);
        }
    }
    pub fn register(self: *@This(), stages: []const Stage, obj: T) void {
        for (stages) |stage| {
            self.middleware[@intFromEnum(stage)].append(obj) catch unreachable;
        }
    }
};

pub const IEndpoint = struct {
    pub const Ctx = struct {
        req: *Request,
        _client: StreamServer.Connection,
        http_server: *Self,
        send: *const fn (self: *Ctx, data: []const u8) anyerror!usize = send,
        send_headers: *const fn (self: *Ctx, data: []const u8) anyerror!usize = send_headers,
        // writer: BufferWriter(4096, std.net.Stream.Writer, Self, middleware_callback).Writer,
        pub fn send_headers(self: *Ctx, data: []const u8) !usize {
            if (!Strings.eql(data[data.len - 4 .. data.len], "\r\n\r\n")) {
                log_print(.Err, "Sent invalid header set via 'send_headers'\n{s}\n", .{data});
                @panic("Invalid headers send\n");
            }
            return try self._client.stream.write(data);
        }

        pub fn send_headers_fmt(self: *Ctx, comptime fmt: []const u8, args: anytype) !usize {
            var buffer: [1024]u8 = undefined;
            const data = try Strings.format(&buffer, fmt, args);
            return try send_headers(self, data);
        }

        pub fn send(self: *Ctx, data: []const u8) !usize {
            return try self._client.stream.write(data);
        }

        pub fn send_simple_response_fmt(self: *Ctx, comptime fmt: []const u8, args: anytype, content: []const u8) !void {
            self.send_headers_fmt(fmt, args);
            self.send(content);
        }

        pub fn send_simple_response(
            self: *Ctx,
            status: []const u8,
            content_type: []const u8,
            content: []const u8,
        ) !void {
            _ = try self.send_headers_fmt(SIMPLE_RESPONSE, .{ status, content_type, content.len });
            _ = try send(self, content);
        }

        pub fn send_chunked(self: *Ctx, file: std.fs.File, content_type: []const u8, content_length: usize) !void {
            // TODO Send to thread queue so it doesn't consume a whole thread
            // format and send the http header
            var buffer: [1024]u8 = undefined;
            _ = try self.send_headers_fmt(TEMPLATE_CHUNKED_RESPONSE, .{ "200 OK", content_type, content_length });
            // Reserve room for the size octet and CRNL
            var chunk_header = buffer[0..10];
            chunk_header[8] = '\r';
            chunk_header[9] = '\n';
            const chunk = buffer[10 .. buffer.len - 2];
            var amount_read: usize = try file.read(chunk);
            while (true) : (amount_read = try file.read(chunk)) {
                const last = amount_read < chunk.len;
                var index: usize = chunk_header.len - 1 - 2;
                // Convert to hex
                var base10 = amount_read;
                while (base10 > 0) : (index -= 1) {
                    chunk_header[index] = hexchars[base10 % 16];
                    base10 = @divTrunc(base10, 16);
                }
                index += 1; // TODO the loop should handle this
                const pkt_start = index;
                const end = index + (10 - index) + amount_read + 2;
                buffer[end - 2] = '\r';
                buffer[end - 1] = '\n';
                // TODO std.os.sendfile
                _ = try self._client.stream.write(buffer[pkt_start..end]);
                if (last) break;
            }
            _ = try self._client.stream.write("0\r\n\r\n");
        }

        pub fn send_206(self: *Ctx, file: std.fs.File, approx_chunk_size: usize, content_type: []const u8) !bool {
            var buffer: [1024]u8 = undefined;
            return try serve_partial_response(self._client, self.req, file, &buffer, approx_chunk_size, content_type);
        }

        pub fn send_file(self: *Ctx, file: std.fs.File, comptime header_template: []const u8, args: anytype) !void {
            try file.seekTo(0);
            _ = try self.send_headers_fmt(header_template, args);

            const START = std.time.nanoTimestamp();
            const total_transfered: usize = (try file.stat()).size;
            // syscall
            _ = try std.posix.sendfile(self._client.stream.handle, file.handle, 0, 0, &.{}, &.{}, 0);

            const END = std.time.nanoTimestamp();
            const total_time_sec = @as(f64, @floatFromInt(END - START)) / 1_000_000_000.0;

            log_print(.Info, "Bandwidth: {d:.2}MB/s\n", .{@as(f64, @floatFromInt(total_transfered)) / 1000.0 / 1000.0 / total_time_sec});
        }

        pub fn reaccept(self: *Ctx) !void {
            try self.http_server.reaccept(self._client);
        }
    };
    name: []const u8,
    acceptFn: *const fn (self: *IEndpoint, ctx: *Ctx) bool,

    pub fn accept(self: *IEndpoint, ctx: *Ctx) bool {
        return self.acceptFn(self, ctx);
    }
};

pub var ERROR_LOG_FILE: ?std.fs.File = null;
pub var LOGGING: enum { None, Sec, Err, Info, All } = .Err;
pub var PRINT_ACCEPTED_ENDPOINT: bool = false;
handlers: std.ArrayList(*IEndpoint) = undefined,
allocator: std.mem.Allocator = undefined,
threads: @import("Threading.zig") = undefined,
http_config: HTTP_CONFIG = undefined,
server: StreamServer = undefined,

pub fn start(self: *Self) !void {
    var addr = try std.net.Address.parseIp(self.http_config.ip, self.http_config.port);
    self.server = addr.listen(.{ .reuse_port = self.http_config.reuse_port }) catch |err| switch (err) {
        // error.AddressInUse => {
        //     if (self.http_config.gen_port) {
        //         var i: u16 = 1;
        //         var addr = try std.net.Address.parseIp(self.http_config.ip, self.http_config.port + i);
        //         while (self.server.listen(addr) == error.AddressInUse) : ({
        //             i += 1;

        //             addr = try std.net.Address.parseIp(self.http_config.ip, self.http_config.port + i);
        //         }) {}
        //     } else {
        //         return err;
        //     }
        // },
        else => {
            return err;
        },
    };

    // self.server = StreamServer.init(.{ .reuse_port = self.http_config.reuse_port });
    defer self.server.deinit();
    LOGGING = @enumFromInt(@min(self.http_config.logging, @intFromEnum(@TypeOf(LOGGING).All)));
    if (self.http_config.log_file) |p| {
        ERROR_LOG_FILE = try std.fs.cwd().createFile(p, .{});
    }
    defer if (ERROR_LOG_FILE) |f| f.close();

    std.debug.print("\nListeneing on http://{?} |\n", .{self.server.listen_address});
    try self.threads.start_threads();
    defer self.threads.deinit();
    while (true) {
        // Accept connection

        const client: StreamServer.Connection = try self.server.accept();

        try self.threads.append(client);
    }
}

pub fn reaccept(self: *Self, client: StreamServer.Connection) !void {
    try self.threads.resubmit(client);
}

pub fn mainloop_noerr(server: *Self, client: StreamServer.Connection) void {
    mainloop(server, client) catch |err| {
        defer {
            client.stream.close();
        }
        switch (err) {
            else => {
                if (@errorReturnTrace()) |ert| std.debug.dumpStackTrace(ert.*);
                return;
            },
        }
    };
}

pub fn mainloop(server: *Self, client: StreamServer.Connection) !void {
    {
        var stagedata: MiddlewareBox.StageData = .{ .Pre_request = .{ .server = server, .client = client } };
        if (MiddlewareBox.singleton) |*box| {
            switch (box.run_stage(MiddlewareBox.Stage.Pre_request, &stagedata)) {
                .Pass => {},
                .Cancel => {
                    client.stream.close();
                    return;
                },
                .Handled => {},
            }
        }
    }
    var request: [1024]u8 = undefined;
    std.posix.setsockopt(client.stream.handle, std.posix.SOL.SOCKET, std.posix.SO.RCVTIMEO, &std.mem.toBytes(std.posix.timeval{ .tv_sec = 5, .tv_usec = 0 })) catch unreachable;

    var req: Request = Request.recv(client, &request, server.allocator) catch |err| {
        defer client.stream.close();
        var status: ?[]const u8 = null;
        switch (err) {
            error.EOF => {},
            error.InvalidVersion => status = "418 I'm a teapot",
            error.OutOfSpace, error.PathTooLong => {
                status = "431 Request Header Fields Too Large";
            },
            else => {
                status = "500 Internal Server Error";
                if (err == error.Loris) {
                    if (DataTypes.IpBlocker.SINGLETON) |*blocker| {
                        try blocker.insert(client.address.in.sa.addr);
                        log_print(.Sec, "Detected[{any}] Slowloris\n", .{client.address});
                    }
                }
            },
        }
        if (status) |s| {
            log_print(.Err, "err[{any}] receiving request[{any}] {s}\n", .{ err, client.address, request });
            var _ctx: IEndpoint.Ctx = .{ .req = undefined, ._client = client, .http_server = server };
            _ctx.send_simple_response(s, "application/json", "{\"error\": \"error\"}") catch {};
        }
        return;
    };
    defer req.deinit();
    // var writer = bufferedWriter(4096, client.writer(), Self, server, middleware_callback);
    var ctx: IEndpoint.Ctx = .{
        .req = &req,
        ._client = client,
        .http_server = server,
        // .writer = writer.writer(),
    };
    if (MiddlewareBox.singleton) |*box| {
        var data: MiddlewareBox.StageData = .{ .Post_request = &ctx };
        switch (box.run_stage(MiddlewareBox.Stage.Post_request, &data)) {
            .Pass => {},
            .Cancel => {
                try ctx.send_simple_response("500 Server Error", "text/plain", "Middleware rejected connection.");
                client.stream.close();
                return;
            },
            .Handled => {},
        }
    }

    try req.parse_url_mut();
    for (server.handlers.items) |endpoint| {
        if (endpoint.accept(&ctx)) {
            if (PRINT_ACCEPTED_ENDPOINT) {
                std.debug.print("{s} accepted Request.\n", .{endpoint.name});
            }
            // if (ctx.writer.end != 0) ctx.writer.flush();
            return;
        }
    }

    try ctx.send_simple_response("501 Not Implemented", "text/plain", "This server cannont handle that request");
    client.stream.close();
}

pub fn init(self: *Self, allocator: std.mem.Allocator, http_config: HTTP_CONFIG) void {
    self.handlers = std.ArrayList(*IEndpoint).init(allocator);
    self.allocator = allocator;
    self.http_config = http_config;
    self.threads = @import("Threading.zig").create(self, allocator, http_config.threads - 1);
}

pub fn deinit(self: *Self) void {
    self.handlers.deinit();
}

pub fn add_endpoint(self: *Self, handler: *IEndpoint) void {
    self.handlers.append(handler) catch unreachable;
}
