const std = @import("std");
const DataTypes = @import("data_types.zig");
const Args = @import("args.zig");
const parse = @import("args.zig").parse;
const HTTP_CONFIG = Args.HTTP_CONFIG;
const WSEndpoint = @import("endpoints/websocket/WebSocket.zig");
const HttpServer = @import("HttpServer.zig");
const windows: bool = @import("builtin").os.tag == .windows;

const IpBlocker = DataTypes.IpBlocker;
const MiddlewareBox = HttpServer.MiddlewareBox;
pub const Endpoints = @import("endpoints.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    var config: HTTP_CONFIG = parse(allocator);
    var server = HttpServer{};
    server.init(allocator, config);

    if (IpBlocker.SINGLETON) |_| {} else {
        IpBlocker.SINGLETON = .{};
        IpBlocker.SINGLETON.?.init(allocator);
    }
    defer DataTypes.IpBlocker.SINGLETON.?.deinit();
    MiddlewareBox.singleton = undefined;
    MiddlewareBox.singleton.?.init(allocator);

    var middleware_ipblocker: MiddlewareBox.Middleware = .{ .interceptFn = struct {
        pub fn intercept(self: *MiddlewareBox.Middleware, stage: MiddlewareBox.Stage, data: *MiddlewareBox.StageData) MiddlewareBox.StageResponse {
            switch (stage) {
                .Pre_request => {
                    if (IpBlocker.SINGLETON.?.contains(data.Pre_request.client.address.in.sa.addr)) {
                        return .Cancel;
                    }
                    return .Pass;
                },
                else => return .Pass,
            }
            _ = self;
        }
    }.intercept };
    MiddlewareBox.singleton.?.register(&.{MiddlewareBox.Stage.Pre_request}, &middleware_ipblocker);
    var fileserver = Endpoints.FileSystemEP.FileEP(.{}){
        .alloc = allocator,
        .config = &config,
    };
    server.add_endpoint(&fileserver.endpoint);

    server.start() catch |err| switch (err) {
        error.AddressInUse => {
            std.debug.print("{any}.\n", .{err});
            std.posix.exit(0);
        },
        else => {
            return err;
        },
    };
}

test {
    // std.debug.print("\nhash {s} built on {s}", .{ build_info.build_hash, build_info.build_time });
    const ChatServer = @import("endpoints/websocket/WebSocketChat.zig");
    var config: HTTP_CONFIG = parse(std.testing.allocator);
    var server = HttpServer{};
    HttpServer.LOGGING = .All; //.Err;
    server.init(std.testing.allocator, config);
    var websocket = if (!windows) WSEndpoint{
        .options = .{
            .protocol = null,
            .path = null,
            .websocket_handle = ChatServer.web_socket_chat,
        },
    } else {};

    if (!windows) server.add_endpoint(&websocket.endpoint);
    var thread = if (!windows) try std.Thread.spawn(.{}, ChatServer.worker, .{}) else {};
    defer if (!windows) thread.join();

    var redirect = Endpoints.RedirectEP{ .path = struct {
        fn asdf(req: *DataTypes.Request) bool {
            return DataTypes.Strings.starts_with("///", req.path);
        }
    }.asdf, .destination = "https://google.com/" };
    server.add_endpoint(&redirect.endpoint);
    var fileserver = Endpoints.FileSystemEP.FileEP(.{}){
        .alloc = std.testing.allocator,
        .config = &config,
    };
    server.add_endpoint(&fileserver.endpoint);

    var epupload = Endpoints.UploadEP{};
    server.add_endpoint(&epupload.endpoint);
    var tr = Endpoints.RedirectEP{
        .path = struct {
            pub fn a(req: *DataTypes.Request) bool {
                return DataTypes.Strings.starts_with("////", req.path);
            }
        }.a,
        .destination = "https://yahoo.com/",
    };

    var wrapped =
        Endpoints.WrappedEP{
        .wrapped = &tr.endpoint,
        .acceptsFn = null,
    };
    server.add_endpoint(&wrapped.endpoint);
    // TODO Nested

    // var theater = @import("endpoints/TheaterEP.zig"){
    //     .alloc = allocator,
    //     .mutex = std.Thread.Mutex{},
    //     .route = "/room",
    // };
    // server.add_endpoint(&theater.endpoint);
    server.start() catch |err| switch (err) {
        error.AddressInUse => {
            std.debug.print("{any}.\n", .{err});
            std.posix.exit(0);
        },
        else => {
            return err;
        },
    };
}
