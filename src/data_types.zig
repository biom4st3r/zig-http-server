const std = @import("std");

pub const FdCache = struct {
    const Node = struct {
        next: ?*Node,
        file: std.fs.File,
        // stamp: std.time.timestamp(),
    };
    map: std.StringHashMap(*Node),
    alloc: std.mem.Allocator,
    pub fn poll(self: *@This(), path: []const u8) !*std.fs.File {
        if (self.map.get(path)) |fd| {
            if (fd.next) |n| self.map.put(path, n) else self.map.remove(path);
            fd.next = null;
            return &fd.file;
        }
        var t = try self.alloc.create(Node);
        t.* = .{
            .next = null,
            .file = std.fs.cwd().openFile(path, .{}),
        };
        return &t.file;
    }
    pub fn offer(self: *@This(), file: *std.fs.File, path: []const u8) !void {
        var node = promote(file);
        if (self.map.get(path)) |cnode| {
            node.next = cnode;
        }
        try self.map.put(path, node);
    }
    pub inline fn promote(file: *std.fs.File) *Node {
        return @fieldParentPtr("file", file);
    }
};

/// Callback is called before the buffer is sent to allow modifications
/// If callback sends any part of the buffer it MUST send the whole buffer
/// If callback sends any part of the buffer it MUST return true
pub fn fixedBufferedWriter(
    buffer: []u8,
    comptime CtxType: type,
    ctx: *CtxType,
    comptime callback: *const fn (ctx: *CtxType, buffer: []u8) void,
) FixedBufferWriter(CtxType, callback) {
    return .{
        .ctx = ctx,
        .buffer = buffer,
    };
}

/// Code Ripped from std.io.BufferWriter
/// Callback is called before the buffer is sent to allow modifications
/// If callback sends any part of the buffer it MUST send the whole buffer
/// If callback sends any part of the buffer it MUST return true
pub fn FixedBufferWriter(comptime CtxType: type, comptime pre_flush_callback: *const fn (ctx: *CtxType, buffer: []u8) void) type {
    return struct {
        buffer: []u8,
        end: usize = 0,
        flushes: usize = 0,
        ctx: *CtxType,

        pub const WriterType = std.io.FixedBufferStream([]const u8);
        pub const Error = error{};
        pub const Writer = std.io.Writer(*@This(), Error, write);

        pub fn flush(self: *@This()) void {
            pre_flush_callback(self.ctx, self.buffer[0..self.end]);
            self.end = 0;
            self.flushes += 1;
        }

        pub fn writer(self: *@This()) Writer {
            return .{ .context = self };
        }

        pub fn write(self: *@This(), bytes: []const u8) Error!usize {
            if (self.end + bytes.len > self.buffer.len) {
                // TODO Copy as much at possible into self.bufferb
                self.flush();
                if (bytes.len > self.buffer.len) {
                    var start: usize = 0;
                    while (bytes.len - start > self.buffer.len) : (start += self.buffer.len) {
                        @memcpy(self.buffer, bytes[start .. start + self.buffer.len]);
                        pre_flush_callback(self.ctx, self.buffer);
                    }
                    @memcpy(self.buffer[0 .. bytes.len - start], bytes[start..]);
                    pre_flush_callback(self.ctx, self.buffer[0 .. bytes.len - start]);
                    return bytes.len;
                }
            }

            const new_end = self.end + bytes.len;
            @memcpy(self.buffer[self.end..new_end], bytes);
            self.end = new_end;
            return bytes.len;
        }
    };
}

/// Callback is called before the buffer is sent to allow modifications
/// If callback sends any part of the buffer it MUST send the whole buffer
/// If callback sends any part of the buffer it MUST return true
pub fn bufferedWriter(
    comptime size: u64,
    writer: anytype,
    comptime CtxType: type,
    ctx: *CtxType,
    callback: *const fn (ctx: @TypeOf(ctx), writer: @TypeOf(writer), buffer: []u8) bool,
) BufferWriter(size, @TypeOf(writer), @TypeOf(ctx), callback) {
    return .{
        .ctx = ctx,
        .writer = writer,
    };
}
/// Code Ripped from std.io.BufferWriter
/// Callback is called before the buffer is sent to allow modifications
/// If callback sends any part of the buffer it MUST send the whole buffer
/// If callback sends any part of the buffer it MUST return true
pub fn BufferWriter(comptime size: usize, comptime WriterType: type, comptime CtxType: type, comptime pre_flush_callback: *const fn (ctx: *CtxType, writer: WriterType, buffer: []u8) bool) type {
    return struct {
        unbuffered_writer: WriterType,
        buf: [size]u8 = undefined,
        end: usize = 0,
        flushes: usize = 0,
        ctx: *CtxType,

        pub const Error = WriterType.Error;
        pub const Writer = std.io.Writer(*@This(), Error, write);

        pub fn flush(self: *@This()) !void {
            if (!pre_flush_callback(self.ctx, self.unbuffered_writer, self.buf[0..self.end])) {
                try self.unbuffered_writer.writeAll(self.buf[0..self.end]);
            }
            self.end = 0;
            self.flushes += 1;
        }

        pub fn writer(self: *@This()) Writer {
            return .{ .context = self };
        }

        pub fn write(self: *@This(), bytes: []const u8) Error!usize {
            if (self.end + bytes.len > self.buf.len) {
                try self.flush();
                if (bytes.len > self.buf.len)
                    return self.unbuffered_writer.write(bytes);
            }

            const new_end = self.end + bytes.len;
            @memcpy(self.buf[self.end..new_end], bytes);
            self.end = new_end;
            return bytes.len;
        }
    };
}

pub const ResponseBuilder = struct {
    response: []const u8,

    pub fn add_code(comptime version: []const u8, comptime code: []const u8) @This() {
        return .{ .response = version ++ " " ++ code ++ " " ++ "\r\n" };
    }
    pub fn content_type(comptime self: @This(), comptime value: []const u8) @This() {
        return .{ .response = self.response ++ "Content-Type: " ++ value ++ "\r\n" };
    }
    pub fn connection(comptime self: @This(), comptime value: enum { @"keep-alive", close }) @This() {
        return .{ .response = self.response ++ "Connection: " ++ @tagName(value) ++ "\r\n" };
    }
    pub fn add_field(comptime self: @This(), comptime name: []const u8, comptime value: []const u8) @This() {
        return .{ .response = self.response ++ name ++ ": " ++ value ++ "\r\n" };
    }
    pub fn content_length(comptime self: @This(), comptime value: []const u8) @This() {
        return .{ .response = self.response ++ "Content-Length: " ++ value ++ "\r\n" };
    }
    pub fn location(comptime self: @This(), comptime value: []const u8) @This() {
        return .{ .response = self.response ++ "Location: " ++ value ++ "\r\n" };
    }
    pub fn end(comptime self: @This()) *const [self.response.len + 2]u8 {
        return @as(*const [self.response.len + 2]u8, self.response ++ "\r\n");
    }
};

pub const Strings = struct {
    const StringErr = error{
        OUT_OF_SPACE,
    };

    pub fn sliceFromSentinel(array: [:0]const u8) []const u8 {
        for (array, 0..) |c, i| {
            if (c == 0) {
                return array[0..i];
            }
        }
        unreachable;
    }

    pub fn copyInto(comptime size: usize, source: []const u8) StringErr![size:0]u8 {
        if (source.len >= size) return StringErr.OUT_OF_SPACE;
        var t: [size:0]u8 = undefined;
        @memcpy(t[0..source.len], source);
        t[source.len] = 0;
        return t;
    }

    pub fn runtime_fmt(writer: anytype, content: []const u8, args: []const []const u8) !void {
        var str = content;
        var arg: u64 = 0;
        var i: u64 = 0;
        var escaping = false;
        while (i < str.len) : (i += 1) {
            if (escaping) {
                while (str[i - 1] == '}' and str[i] == '}' and str[i + 1] != '}') : (i += 1) {
                    if (i + 1 >= str.len) return error.InvalidFormat;
                }
                str = str[i + 2 ..];
                escaping = false;
            } else if (str[i] == '{') {
                if (str[i + 1] == '{') {
                    escaping = true;
                    continue;
                }
                // const start = i;
                _ = try writer.print("{s}{s}", .{ str[0..i], args[arg] });
                arg += 1;
                while (str[i] != '}') : (i += 1) {}
                // var placeholder = str[start + 1 .. i];
                str = str[i + 1 ..];
                i = 0;
            }
        }
        _ = try writer.write(str);
    }

    pub fn split(buffer: []const u8, search: []const u8) std.mem.SplitIterator(u8, .sequence) {
        return std.mem.splitSequence(u8, buffer, search);
    }

    pub fn splitReverse(buffer: []const u8, search: []const u8) std.mem.SplitBackwardsIterator(u8, .sequence) {
        return std.mem.splitBackwardsSequence(u8, buffer, search);
    }

    // pub fn contains(buffer: []const u8, search: []const u8) bool {
    //     if (Strings.find(search, buffer)) |_| return true;
    //     // if (std.mem.eql(u8, buffer, search)) {
    //     //     return true;
    //     // }
    //     return false;
    // }

    pub fn containsIgnoreCase(buffer: []const u8, search: []const u8) bool {
        if (std.ascii.indexOfIgnoreCase(buffer, search)) |_| {
            return true;
        }
        return false;
    }

    pub fn containsAnyIgnoreCase(buffer: []const u8, search: []const []const u8) bool {
        for (search) |needle| {
            if (std.ascii.indexOfIgnoreCase(buffer, needle)) |_| {
                return true;
            }
        }
        return false;
    }

    pub fn format(buffer: []u8, comptime fmt: []const u8, args: anytype) ![]u8 {
        return try std.fmt.bufPrint(buffer, fmt, args);
    }

    pub fn writeAll(buffer: []u8, vals: []const []const u8) StringErr![]u8 {
        var data = buffer;
        var i = 0;
        const used: u64 = 0;
        while (i < vals.len) : (i += 1) {
            if (data.len < vals[i].len) return StringErr.OUT_OF_SPACE;
            std.mem.copy(u8, data, vals[i]);
            data = data[vals[i].len..];
        }
        return buffer[used..];
    }

    pub fn replace_all(buffer: []u8, haystack: []const u8, needle: []const u8, replace: []const u8) StringErr![]u8 {
        var buffer_tail = 0;
        var haystack_tail = 0;
        while (true) {
            const i = find(needle, haystack[haystack_tail..]) orelse break;
            if (buffer[buffer_tail..].len < haystack[haystack_tail..i]) return StringErr.OUT_OF_SPACE;
            std.mem.copy(u8, buffer[buffer_tail..], haystack[haystack_tail..i]);
            buffer_tail += i;
            if (buffer[buffer_tail..].len < replace.len) return StringErr.OUT_OF_SPACE;
            std.mem.copy(u8, buffer[buffer_tail..], replace);
            buffer_tail += replace.len;
            haystack_tail += i + needle.len;
        }
        return buffer[0..buffer_tail];
    }

    pub fn eql(a: []const u8, b: []const u8) bool {
        return std.mem.eql(u8, a, b);
    }

    pub fn ends_with(needle: []const u8, haystack: []const u8) bool {
        if (needle.len > haystack.len) return false;
        return std.mem.eql(u8, haystack[(haystack.len - needle.len)..], needle);
    }

    pub fn starts_with(needle: []const u8, haystack: []const u8) bool {
        if (needle.len > haystack.len) return false;
        return std.mem.eql(u8, haystack[0..needle.len], needle);
    }

    // pub var find: *const fn ([]const u8, []const u8) ?usize = find__;

    pub fn find(needle: []const u8, haystack: []const u8) ?usize {
        // return find__3(needle, haystack);
        return find__2(needle, haystack);
    }

    pub fn parse_from_to(haystack: []const u8, from: []const u8, to: []const u8) ?[]const u8 {
        if (find(from, haystack)) |f_index| {
            if (f_index + 1 >= haystack.len) return null; // incase from == to
            if (find(to, haystack[(f_index + 1)..])) |t_index| {
                return haystack[f_index..(t_index + f_index + 1)];
            }
        }
        return null;
    }
};

pub const Connection = enum {
    @"keep-alive",
    close,

    pub fn parse(str: ?[]const u8) ?Connection {
        if (str) |connection| {
            return std.meta.stringToEnum(Connection, connection);
        }
        return null;
    }
};

pub const Method = enum {
    POST,
    GET,
    PUT,
    PATCH,
    DELETE,
    OPTIONS,
    HEAD,
    CONNECT,
    TRACE,

    pub fn parse(str: []const u8) ?Method {
        // if (str) |method| {
        return std.meta.stringToEnum(Method, str);
        // }
        // return null;
    }
};

pub const Version = enum {
    @"HTTP/0.9",
    @"HTTP/1.0",
    @"HTTP/1.1",
    @"HTTP/2.0",
    @"HTTP/3.0",
    pub fn parse(str: []const u8) ?Version {
        // if (str) |method| {
        return std.meta.stringToEnum(Version, str);
        // }
        // return null;
    }
};

pub const UA__Options = enum {
    standard,
    custom,
    none,
};

pub const UserAgent = struct {
    browser: union(UA__Options) {
        standard: Browser,
        custom: []const u8,
        none: void,
    } = .{ .none = {} },
    os: Os = .Unknown,

    pub fn print(self: *const UserAgent) void {
        std.debug.print("Browser: {s} | os: {s}\n", .{ switch (self.browser) {
            .standard => |val| @tagName(val),
            .custom => |val| val,
            .none => |_| "void",
        }, @tagName(self.os) });
    }
    pub fn matches_browser(self: *const UserAgent, match: union(UA__Options) {
        standard: Browser,
        custom: []const u8,
        none: void,
    }) bool {
        return switch (match) {
            .standard => |val| {
                return switch (self.browser) {
                    .standard => |val2| return val == val2,
                    .custom => |_| return false,
                    .none => |_| return false,
                };
            },
            .custom => |val| {
                return switch (self.browser) {
                    .standard => |_| return false,
                    .custom => |val2| Strings.eql(val2, val),
                    .none => |_| return false,
                };
            },
            .none => |_| {
                return switch (self.browser) {
                    .standard => |_| return false,
                    .custom => |_| return false,
                    .none => |_| return true,
                };
            },
        };
    }

    pub fn parse(_useragent: []const u8) !UserAgent {
        var agent: UserAgent = .{};
        var ua = _useragent;
        const is_lying = Strings.starts_with("Mozilla/5.0", ua); // Most browsers
        // std.debug.print("is_lying: {any}\n", .{is_lying});
        if (is_lying) ua = ua["Mozilla/5.0 ".len..] else {
            // Not lying
            var set = false;
            inline for (std.meta.fields(Browser)) |field| {
                // Not sure if this actuially catches any browsers, but just in case.
                if (Strings.starts_with(field.name, ua)) {
                    agent.browser = .{ .standard = @as(Browser, @enumFromInt(field.value)) };
                    set = true;
                    break;
                }
            }
            if (!set) {
                // Try to parse the typical format "TheApplication/The.version"
                if (Strings.find("/", ua)) |version_split| {
                    if (version_split > 15) { // Very long Application name. Likely non-standard useragent
                        agent.browser = .{ .none = {} };
                    } else {
                        // Found "TheApplication" part
                        agent.browser = .{ .custom = ua[0..version_split] };
                        ua = ua[version_split + 1 ..];
                    }
                    if (Strings.find(" ", ua)) |i| {
                        // Has seperate section. Keep scanning
                        ua = ua[i + 1 ..];
                    } else {
                        return agent;
                    }
                }
            }
        }
        if (ua[0] == '(') if (Strings.find(")", ua)) |i| {
            // Possibly found the Operating system section
            // this is also where bot strings live
            const os = ua[1..i];
            var set = false;
            inline for (std.meta.fields(Bots)) |field| {
                if (Strings.find(field.name, os)) |_| {
                    agent.browser = .{ .custom = field.name };
                    set = true;
                    break;
                }
            }
            if (!set)
                inline for (std.meta.fields(Os)) |field| {
                    // Check for known Os strings. This is sorted to try and capture correctly
                    if (Strings.find(field.name, os)) |_| {
                        agent.os = @as(Os, @enumFromInt(field.value));
                        break;
                    }
                };
            ua = ua[i + 1 ..];
        };
        inline for (std.meta.fields(Browser)) |field| {
            // Never scanned for the correct browser. lets check now
            // Browser is also sorted to try to filter out liers
            if (Strings.find(field.name, ua)) |_| {
                agent.browser = .{ .standard = @as(Browser, @enumFromInt(field.value)) };
                break;
            }
        }
        return agent;
    }
};

pub const Os = enum(u4) {
    Android = 0,
    Linux = 1,
    Windows = 2,
    Macintosh = 3,
    iPhone = 4,
    iPad = 5,
    iPod = 6,
    CriOS = 7,
    FxiOS = 8,
    Unknown = 9,
};

pub const Bots = enum(u4) {
    Googlebot,
    @"-Google",
    Bingbot,
    @"Yahoo! Slurp",
    DuckDuckBot,
    Baiduspider,
    YandexBot,
    Konqueror,
    Exabot,
};

pub const Browser = enum(u4) {
    OPR = 0,
    Chromium = 1,
    Chrome = 2,
    Edg = 3,
    Safari = 4,
    Firefox = 5,
};

pub fn find__(needle: []const u8, haystack: []const u8) ?usize {
    if (needle.len == 1) return find__2_needle1(needle, haystack);
    if (needle.len > haystack.len) return null;

    var i: usize = 0;
    while (i <= haystack.len - needle.len) : (i += 1) {
        if (haystack[i] == needle[0]) {
            // if (needle.len == 1) return i;
            var n: usize = 1;
            while (n < needle.len and haystack[i + n] == needle[n]) : (n += 1) {}
            if (needle.len == n) {
                return i;
            }
        }
    }
    return null;
}

pub fn find__2_needle1(needle: []const u8, haystack: []const u8) ?usize {
    var i: usize = 0;
    if (haystack.len == 0) return null;
    while (i <= haystack.len - needle.len) : (i += 1) {
        if (haystack[i] == needle[0]) return i;
    }
    return null;
}

pub fn find__3(needle: []const u8, haystack: []const u8) ?usize {
    if (needle.len == 1) return find__2_needle1(needle, haystack);
    if (needle.len > haystack.len) return null;
    var running: usize = 0;
    // Calc sum of needle chars
    // preload running total of haystack[0..needle length];
    const total = blk: {
        var i: usize = 0;
        for (needle, haystack[0..needle.len]) |c, h| {
            i += c;
            running += h;
        }
        break :blk i;
    };
    var i: usize = 0;
    main: while (i <= haystack.len - needle.len) : ({
        running -= haystack[i];
        running += haystack[i + needle.len];
        i += 1;
        // std.debug.print("runn{d} index{d}\n", .{ running, haystack[i] });
    }) {
        if (running == total) {
            for (needle, haystack[i .. i + needle.len]) |n, h| {
                if (n != h) continue :main;
            }
            return i;
        }
    }
    return null;
}

pub fn find__2(needle: []const u8, haystack: []const u8) ?usize {
    if (needle.len == 1) return find__2_needle1(needle, haystack);
    if (needle.len > haystack.len) return null;
    var i: usize = 0;
    blk: while (i <= haystack.len - needle.len) : (i += 1) {
        if (haystack[i] == needle[0]) {
            for (needle[1..], haystack[i + 1 .. i + needle.len]) |a, b| {
                if (a != b) continue :blk;
            }
            return i;
        }
    }
    return null;
}

pub fn url_decode_mut(val: []u8) ![]u8 {
    var i: usize = 0;
    var o: usize = 0;
    while (i < val.len) : ({
        i += 1;
        o += 1;
    }) {
        var char: u8 = val[i];
        if (char == '%') {
            char = try std.fmt.parseUnsigned(u8, val[i + 1 .. i + 3], 16);
            i += 2;
        }
        val[o] = char;
    }
    return val[0..o];
}

pub const IpBlocker = struct {
    // @setRuntimeSafety(false);
    pub var SINGLETON: ?IpBlocker = null;
    blocked: std.ArrayList(u32) = undefined,
    mutex: std.Thread.Mutex = .{},
    pub fn insert(self: *@This(), ip: u32) !void {
        // std.debug.print("blocking: {d}\n", .{ip});
        for (0..self.blocked.items.len) |i| {
            if (self.blocked.items[i] == ip) return;
            if (self.blocked.items[i] < ip) {
                if (i + 1 >= self.blocked.items.len) break else {
                    self.mutex.lock();
                    defer self.mutex.unlock();
                    try self.blocked.insert(i + 1, ip);
                }
            }
        }
        try self.blocked.append(ip);
    }
    pub fn contains(self: *@This(), ip: u32) bool {
        if (self.blocked.items.len == 0) return false;
        var h: u64 = self.blocked.items.len - 1;
        var l: u64 = 0;
        while (l < h) {
            const m = @divFloor(h + l, 2);
            self.mutex.lock();
            defer self.mutex.unlock();
            if (self.blocked.items[m] < ip)
                l = m + 1
            else if (self.blocked.items[m] > ip)
                h = m
            else
                return true;
        }
        return false;
    }
    pub fn init(self: *@This(), alloc: std.mem.Allocator) void {
        self.blocked = std.ArrayList(u32).init(alloc);
    }
    pub fn deinit(self: *@This()) void {
        self.blocked.deinit();
    }
};
// TODO Split the difference. Cache headers that have been looked up
pub const Request = struct {
    pub const HTTPREQErr = error{
        MALFORMED_HEADER,
    };
    full_request: []const u8,
    data: ?[]const u8 = null,
    method: ?Method,
    path: []u8,
    query: ?[]const u8,
    version: Version,
    pub fn recv(client: std.net.Server.Connection, buffer: []u8, alloc: std.mem.Allocator) !@This() {
        _ = alloc;
        var step: enum { Method, Path, Version, rn, rnrn, done } = .Method;
        // Keeps track of the current segment of the buffer that has useful information
        var inuse: struct { start: u64 = 0, end: u64 = 0 } = .{};
        // ^ this section of the buffer;
        var active_buff: []u8 = buffer[inuse.start..inuse.end];
        var request: @This() = undefined;
        request.data = null;

        const Infrac = struct {
            pub const mild = 1;
            pub const medium = 3;
            pub const hot = 4;
            pub const spicy = 5;
        };
        var loris_counter: u8 = 0;
        const MAX_LORIS = 10;
        while (step != .done) {
            if (IpBlocker.SINGLETON) |*blocker| {
                if (blocker.contains(client.address.in.sa.addr)) return error.Loris;
            }
            if (loris_counter >= MAX_LORIS) return error.Loris;
            switch (step) {
                .Method, .Path, .Version => {
                    // TODO 414 URI Too Long
                    const lookup = switch (step) {
                        .Method, .Path => " ",
                        else => "\r\n",
                    };
                    if (Strings.find(lookup, active_buff)) |i| { // Found the section we were looking for
                        switch (step) {
                            .Method => request.method = Method.parse(active_buff[0..i]),
                            .Path => request.path = active_buff[0..i],
                            .Version => request.version = Version.parse(active_buff[0..i]) orelse return error.InvalidVersion,
                            else => unreachable,
                        }
                        active_buff = buffer[inuse.start + i + lookup.len .. inuse.end];
                        inuse.start += i + lookup.len;
                        step = @enumFromInt(@intFromEnum(step) + 1);
                    } else { // Need more information
                        if (inuse.end == buffer.len) {
                            switch (step) {
                                .Method => return error.InvalidMethod,
                                .Path => return error.PathTooLong,
                                .Version => return error.InvalidVersion,
                                else => unreachable,
                            }
                        }
                        const r = client.stream.read(buffer[inuse.end..]) catch |err| switch (err) {
                            error.WouldBlock => {
                                loris_counter += Infrac.medium;
                                continue;
                            },
                            else => return err,
                        };
                        if (r == 1) loris_counter += Infrac.spicy;

                        loris_counter += Infrac.mild;
                        if (r == 0) return error.EOF;
                        if (r <= 10) loris_counter += Infrac.mild;

                        inuse.end += r;
                        active_buff = buffer[inuse.start..inuse.end];
                    }
                },
                .rn => {
                    // TODO This method loses headers. and path;
                    if (Strings.find("\r\n\r\n", active_buff)) |end| {
                        request.full_request = active_buff[0..end];
                        step = .rnrn;
                    } else {
                        if (inuse.end == buffer.len) {
                            // TODO It might be nice to grow the buffer
                            // or have someway to empty the buffer
                            return error.OutOfSpace;
                            // if (inuse.start == 0)
                            //     return error.OUT_OF_SPACE;

                            // std.mem.copyForwards(u8, buffer[0 .. inuse.end - inuse.start], buffer[inuse.start..inuse.end]);
                            // inuse = .{ .start = 0, .end = inuse.end - inuse.start }; // Off by 1?
                        }
                        inuse.start = inuse.end;
                        inuse.end += client.stream.read(buffer[inuse.end..]) catch |err| switch (err) {
                            error.WouldBlock => {
                                loris_counter += Infrac.medium;
                                continue;
                            },
                            else => return err,
                        };
                        loris_counter += Infrac.mild;
                        active_buff = buffer[inuse.start..inuse.end];
                    }
                },
                .rnrn => {
                    // TODO Read till EOF
                    if (request.get_header("Content-Length")) |l| {
                        // TODO get the body
                        _ = l;
                        return error.NotImplemented;
                        // unreachable;
                    }
                    step = .done;
                    break;
                },
                else => unreachable,
            }
        }
        request.query = if (Strings.find("?", request.path)) |q| request.path[q + 1 ..] else null;
        return request;
    }

    pub fn parse(request: []u8, alloc: std.mem.Allocator) HTTPREQErr!Request {
        _ = alloc;
        const end_of_method = Strings.find(" ", request) orelse return HTTPREQErr.MALFORMED_HEADER;
        var path = Strings.parse_from_to(request[end_of_method..], " ", " ") orelse return HTTPREQErr.MALFORMED_HEADER;
        const query = Strings.find("?", path);
        var http = Strings.parse_from_to(request[end_of_method + path.len ..], " ", "\r\n") orelse return HTTPREQErr.MALFORMED_HEADER;

        return Request{
            .full_request = request,
            .method = Method.parse(request[0..end_of_method]),
            .path = @constCast(path[1 .. query orelse path.len]),
            .query = if (query) |i| path[i + 1 ..] else null,
            // .fragment = if (fragment) |i| path[i + 1 ..] else null,
            .version = Version.parse(http[1..]),
        };
    }

    pub fn get_cookie(self: *@This(), name: []const u8) ?[]const u8 {
        const cookies = self.get_header("Cookie") orelse return null;
        var iter = std.mem.splitSequence(u8, cookies, "; ");
        while (iter.next()) |next| {
            if (Strings.find("=", next)) |idx| {
                if (!Strings.eql(next[0..idx], name)) continue;
                return next[idx + 1 ..];
            }
        }
        return null;
    }

    pub fn get_header(self: *@This(), comptime name: []const u8) ?[]const u8 {
        var iter = std.mem.splitSequence(u8, self.full_request, "\r\n");
        while (iter.next()) |line| {
            if (Strings.starts_with(name, line)) {
                return line[(Strings.find(": ", line) orelse return null) + 2 ..];
            }
        }
        return null;
    }

    pub fn find_segment(self: *@This(), name: []const u8, end: []const u8) ?[]const u8 {
        return Strings.parse_from_to(self.full_request, name, end);
    }

    pub fn get_content_length(self: *@This()) ?u64 {
        if (self.get_header("Content-Length")) |val| {
            return std.fmt.parseUnsigned(u64, val, 0) catch null;
        }
        return null;
    }

    pub fn get_field_value(self: *@This(), name: []const u8, end: []const u8) ?[]const u8 {
        if (self.find_segment(name, end)) |field| {
            return field[name.len..];
        }
        return null;
    }

    pub fn get_browser(self: *@This()) UserAgent {
        return UserAgent.parse(self.get_header("User-Agent") orelse return .{}) catch .{};
    }

    pub fn print(self: *Request) void {
        std.debug.print("{s}\n", .{self.full_request});
    }

    pub fn get_connection(self: *Request) ?Connection {
        if (self.version != .@"HTTP/1.1") return null;
        return Connection.parse(self.get_header("Connection"));
    }
    pub fn deinit(self: *@This()) void {
        _ = self;
    }

    pub inline fn has_range(self: *Request) bool {
        return self.get_range() != null;
    }

    pub inline fn get_range(self: *Request) ?[]const u8 {
        return self.get_header("Range");
    }

    pub fn get_range_no_unit(self: *Request) ?[]const u8 {
        if (self.get_range()) |range| {
            if (Strings.find("=", range)) |i| {
                return self.get_range().?[i + 1 ..];
            }
        }
        return null;
    }
    pub const Range = struct { start: usize, end: ?usize };
    pub fn get_range_parsed(self: *Request) !?Range {
        const range = self.get_range_no_unit() orelse return null;
        if (Strings.find(",", range)) |_| {
            return error.NotSupported;
        }
        const split = Strings.find("-", range) orelse return null;
        // std.debug.print("{d} | {s}", .{ split, range });
        const parsed = Range{
            .start = try std.fmt.parseInt(usize, range[0..split], 0),
            .end = if (split + 1 == range.len) null else try std.fmt.parseInt(usize, range[split + 1 ..], 0),
        };

        return parsed;
    }
    pub fn parse_url_mut(self: *Request) !void {
        self.path = try url_decode_mut(self.path);
    }
};
