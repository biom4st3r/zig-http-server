// up to 30% reduction

const ACCEPTED_ENDING = ";}{>\n,";

const Strings = @import("data_types.zig").Strings;

pub fn check_for_warn(comptime data: []const u8, i: usize) void {
    if (data.len > i + 1) {
        if (data[i + 1] == '\n') {
            if (Strings.find(data[i .. i + 1], ACCEPTED_ENDING)) |_| {} else {
                @compileLog("Warning: line not ended with '", ACCEPTED_ENDING, "'@", i);
            }
        }
    }
}

pub fn shrink_embed(comptime data: []const u8, comptime lex: bool) type {
    @setEvalBranchQuota(2_000_000);
    comptime {
        var shunk: [data.len]u8 = [_]u8{'@'} ** data.len;
        var i = 0;
        var o = 0;
        while (i < data.len) : ({
            i += 1;
            o += 1;
        }) {
            if (lex) check_for_warn(data, i);
            if (data[i] == ' ' and (i + 1 < data.len)) {
                switch (data[i + 1]) {
                    ' ' => {
                        o -= 1;
                        i += 1;
                    },
                    '(' => o -= 1,
                    '{' => o -= 1,
                    '=' => o -= 1,
                    '!' => o -= 1,
                    '<' => o -= 1,
                    '>' => o -= 1,
                    '+' => o -= 1,
                    '-' => o -= 1,
                    '}' => o -= 1,
                    ')' => o -= 1,
                    else => shunk[o] = data[i],
                }
            } else if (data[i] == '\n') {
                o -= 1;
            } else if (data[i] == '\r') {
                o -= 1;
            } else if (data[i] < '!' and data[i] != ' ') {
                o -= 1;
            } else if (data[i] > 126 and data[i] != ' ') {
                o -= 1;
            } else shunk[o] = data[i];
            if ((i + 1 < data.len) and data[i + 1] == ' ') {
                switch (data[i]) {
                    '=' => i += 1,
                    ',' => i += 1,
                    ';' => i += 1,
                    ':' => i += 1,
                    '<' => i += 1,
                    '>' => i += 1,
                    '+' => i += 1,
                    '-' => i += 1,
                    '}' => i += 1,
                    '{' => i += 1,
                    ')' => i += 1,
                    else => {},
                }
            }
        }
        var out: [o]u8 = undefined;
        for (shunk[0..o], 0..) |c, index| {
            out[index] = c;
        }
        return struct {
            pub const val = out;
        };
    }
}
