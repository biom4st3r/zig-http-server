pub const RedirectEP = @import("endpoints/RedirectEP.zig");
pub const UploadEP = @import("endpoints/UploadEndpoint.zig");
pub const FileSystemEP = @import("endpoints/fileserver/HttpFileServer.zig");
pub const WebSocketEP = @import("endpoints/websocket/WebSocket.zig");
pub const NestedEP = @import("endpoints/NestedEP.zig");
pub const WrappedEP = @import("endpoints/WrappedEP.zig");
pub const HostFilterEP = @import("endpoints/HostEndpoint.zig");
