const std = @import("std");
const StreamServer = std.net.Server;
const DataTypes = @import("../../data_types.zig");
const Request = DataTypes.Request;
const Strings = DataTypes.Strings;
const Args = @import("../../args.zig");
const HTTP_CONFIG = Args.HTTP_CONFIG;
const ResponseBuilder = DataTypes.ResponseBuilder;

const build_info = @import("build_info");

const HttpServer = @import("../../HttpServer.zig");
const IEndpoint = HttpServer.IEndpoint;

pub const FileEPSettings = struct {
    body_buffer_size: u64 = 4096,
    path_buffer_size: u64 = 256,
    serve_directories: bool = true,
    directory_settings: DirectorySettings = .{},
    content_type_provider: *const fn (file_name: []const u8) []const u8 = file_suffix_to_mime_type, // TODO
    large_file_limit: u64 = 1024 * 1024 * 10, // 10MB
    partial_content_chunk_size: u64 = 1024 * 200,
};

pub fn FileEP(comptime settings: FileEPSettings) type {
    return struct {
        const Self = @This();
        endpoint: IEndpoint = .{ .name = "FileEP", .acceptFn = accept },
        config: *const HTTP_CONFIG,
        alloc: std.mem.Allocator,

        fn accept_err(self: *Self, ctx: *IEndpoint.Ctx) !void {
            const req = ctx.req;
            var buffer: [settings.body_buffer_size]u8 = undefined;
            var _path_buffer: [settings.path_buffer_size]u8 = undefined;
            var path_buffer: []u8 = &_path_buffer;
            // If the path is too long then allocate a buffer for it
            const relative_path: []u8 = Strings.format(path_buffer, "{s}{s}", .{ self.config.root_path, req.path }) catch relative_path_fix: {
                const needed_size = self.config.root_path.len + req.path.len;

                HttpServer.log_print(.Info, "Path request to long. Allocating {d}!\n", .{needed_size});
                path_buffer = self.alloc.alloc(u8, self.config.root_path.len + req.path.len) catch return error.FailedCreateRelPath;

                break :relative_path_fix Strings.format(path_buffer, "{s}{s}", .{ self.config.root_path, req.path }) catch return error.FailedFormatRelPath;
            };
            defer {
                // Check if the path buffer needs deallocated by checking if ptrs match
                if (path_buffer.ptr != @as([]u8, &_path_buffer).ptr) {
                    self.alloc.free(path_buffer);
                }
            }

            const realpath = std.fs.cwd().realpath(relative_path, &buffer) catch |err| switch (err) {
                error.NotDir => {
                    return error.FileNotFound;
                },
                else => {
                    return err;
                },
            };

            // Check for malicous requests
            if (!Strings.starts_with(self.config.root_path, realpath)) {
                HttpServer.log_print(.Sec, "Blocked[{any}] Directory Transversal[{s}]\n", .{ ctx._client.address, realpath });
                return error.AttemptedDirectoryTransversal;
            }

            if (is_dir(std.fs.cwd(), relative_path)) {
                if (!settings.serve_directories) {
                    return error.FileNotFound;
                }
                HttpServer.log_print(.All, "Building Directory body\n", .{});
                serve_directory(ctx, self.alloc, relative_path, comptime settings.directory_settings) catch |err| {
                    HttpServer.log_print(.Err, "failed to serve directory[{s}]: {any}\n", .{ relative_path, err });
                    return err;
                };
            } else {
                serve_file(ctx, relative_path, &buffer, settings.large_file_limit, settings.partial_content_chunk_size) catch |err| switch (err) {
                    error.FileNotFound => {
                        return err;
                    },
                    else => {
                        HttpServer.log_print(.Err, "Failed to serve file[{s}]: {any}", .{ relative_path, err });
                        return err;
                    },
                };
            }
        }

        pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
            const self: *Self = @fieldParentPtr("endpoint", endpoint);
            if (!(ctx.req.method == .GET)) return false;
            accept_err(self, ctx) catch |err| {
                var code: []const u8 = "500 Internal Server Error";
                var msg: []const u8 = "{\"error\":\"Sorry '^_^\"}";
                switch (err) {
                    error.NameTooLong => {
                        code = "400 Bad Request";
                        msg = "{\"error\":\"file_name_too_long\"}";
                    },
                    error.FileNotFound, error.AttemptedDirectoryTransversal => {
                        code = "404 Not Found";
                        msg = "{\"error\":\"file_not_found\"}";
                    },
                    error.BrokenPipe => {
                        code = "";
                        msg = "";
                        HttpServer.log_print(.Err, "Broken Pipe\n", .{});
                    },
                    error.AccessDenied => {
                        code = "403 Forbidden";
                        msg = "{\"error\":\"access_denied\"}";
                    },
                    else => {
                        if (@errorReturnTrace()) |trace| std.debug.dumpStackTrace(trace.*);
                        ctx.req.print();
                    },
                }
                if (code.len > 0)
                    ctx.send_simple_response(code, "application/json", msg) catch {};
                ctx._client.stream.close();
                return true;
            };

            defer if (ctx.req.get_connection() == .@"keep-alive") {
                ctx.reaccept() catch ctx._client.stream.close();
                HttpServer.log_print(.All, "Reaccepted\n", .{});
            } else {
                ctx._client.stream.close();
            };
            return true;
        }
    };
}

pub fn serve_file(ctx: *IEndpoint.Ctx, path: []u8, buffer: []u8, large_file_limit: u64, partial_content_chunk: u64) !void {
    _ = buffer;
    var file = std.fs.cwd().openFile(path, .{}) catch |err| return err;
    defer file.close();

    const file_name = if (std.mem.lastIndexOf(u8, path, "/")) |index| path[index..] else return error.InvalidFileName;

    var file_type: []u8 = if (std.mem.lastIndexOf(u8, file_name, ".")) |index|
        file_name[index..]
    else
        &dummy;
    file_type = std.ascii.lowerString(file_type, file_type);
    //https://www.iana.org/assignments/media-types/media-types.xhtml
    const content_type = file_suffix_to_mime_type(file_type);

    if (ctx.req.has_range()) {
        HttpServer.log_print(.All, "Serving partial content: {s}\n", .{file_name});
        const chunkSize = partial_content_chunk;
        if (try ctx.send_206(file, chunkSize, content_type)) return;
        // if (try HttpServer.serve_partial_response(client, req, file, buffer, chunkSize, content_type)) return;
    }

    const stats = try file.stat();

    if (stats.size < large_file_limit) {
        HttpServer.log_print(.Info, "Serve small file: {s}\n", .{file_name});
        try ctx.send_file(file, HttpServer.TEMPLATE_RESPONSE, .{ "200 OK", content_type, (try file.stat()).size });
        // try send_whole_file(client, file, content_type, buffer);

        // serve file
    } else {
        HttpServer.log_print(.Info, "Serve large file: {s}\n", .{file_name});
        try ctx.send_chunked(file, content_type, stats.size);
        // try HttpServer.send_chunked_response(
        //     client,
        //     stats.size,
        //     buffer,
        //     "200 OK",
        //     content_type,
        //     file,
        //     std.fs.File.readAll,
        // );

        // serve large file
    }
}

pub const DirectorySettings = struct {
    header_is_filepath: bool = false,
    header: []const u8 = &shrink(@embedFile("directory_body_head2.html"), false).val,
    file_entry_is_filepath: bool = false,
    file_entry: []const u8 = @embedFile("file_link2.html"),
    folder_entry_is_filepath: bool = false,
    folder_entry: []const u8 = @embedFile("folder_link2.html"),
    footer_is_filepath: bool = false,
    footer: []const u8 = &shrink(@embedFile("directory_body_tail2.html"), true).val,
    fn read_file(path: []const u8, allocator: std.mem.Allocator) ![]u8 {
        var file = try std.fs.cwd().openFile(path, .{});
        defer file.close();
        return try file.readToEndAlloc(allocator, 1024 * 1024 * 5);
    }
    pub fn eval(self: *const @This(), allocator: std.mem.Allocator) !@This() {
        return .{
            .header = if (!self.header_is_filepath) self.header else try read_file(self.header, allocator),
            .file_entry = if (!self.file_entry_is_filepath) self.file_entry else try read_file(self.file_entry, allocator),
            .folder_entry = if (!self.folder_entry_is_filepath) self.folder_entry else try read_file(self.folder_entry, allocator),
            .footer = if (!self.footer_is_filepath) self.footer else try read_file(self.footer, allocator),
        };
    }
};

pub fn serve_directory(ctx: *IEndpoint.Ctx, alloc: std.mem.Allocator, path: []const u8, comptime __settings: DirectorySettings) !void {
    const settings = try __settings.eval(alloc);
    defer {
        if (__settings.header_is_filepath) alloc.free(settings.header);
        if (__settings.file_entry_is_filepath) alloc.free(settings.file_entry);
        if (__settings.folder_entry_is_filepath) alloc.free(settings.folder_entry);
        if (__settings.footer_is_filepath) alloc.free(settings.footer);
    }
    var iterator = try std.fs.cwd().openDir(path, .{ .iterate = true });

    defer iterator.close();
    var iter = iterator.iterate();
    // * hash map cache/lookup is slower than rechecking is_dir
    var content_size: u64 = settings.header.len + settings.footer.len;
    var lookup: [1024]bool = comptime std.mem.zeroes([1024]bool); // TODO This limits a folder to 1024 items
    var i: u16 = 0;
    // Find content length. TODO Make less expensive
    while (try iter.next()) |*file| : (i += 1) {
        switch (file.kind) {
            .file => {
                content_size += settings.file_entry.len;
            },
            .directory => {
                content_size += settings.folder_entry.len;
                lookup[i] = true;
            },
            else => {
                if (is_dir(iterator, file.name)) {
                    content_size += settings.folder_entry.len;
                    lookup[i] = true;
                } else {
                    content_size += settings.file_entry.len;
                }
            },
        }
        content_size = content_size - 6 + (file.name.len * 2);
    }
    iter.reset();

    var buff: [1024]u8 = undefined;

    // _ = try writer.print(HttpServer.TEMPLATE_RESPONSE, .{ "200 OK", "text/html", content_size });
    // try buffwriter.flush();
    _ = try ctx.send_headers_fmt(HttpServer.TEMPLATE_RESPONSE, .{ "200 OK", "text/html", content_size });

    const T = struct {
        ctx: *IEndpoint.Ctx,
        pub fn send(self: *@This(), data: []const u8) void {
            _ = self.ctx.send(self.ctx, data) catch unreachable;
        }
    };
    var t: T = .{ .ctx = ctx };
    var buffer = DataTypes.fixedBufferedWriter(&buff, T, &t, T.send);
    var writer = buffer.writer();
    _ = try writer.write(settings.header);
    i = 0;
    while (try iter.next()) |*file| : (i += 1) {
        const is_directory = lookup[i];

        if (is_directory)
            _ = try Strings.runtime_fmt(writer, settings.folder_entry, &[_][]const u8{ file.name, file.name })
            // _ = try writer.print(settings.folder_entry, .{ file.name, file.name })
        else
            _ = try Strings.runtime_fmt(writer, settings.file_entry, &[_][]const u8{ file.name, file.name });
        // _ = try writer.print(settings.file_entry, .{ file.name, file.name });
    }
    _ = try writer.write(settings.footer);

    buffer.flush();
}

const shrink = @import("../../comptime_shrink.zig").shrink_embed;

// TODO Don't think i can do anything about this, but
// This is the most expensive function
pub fn is_dir(dir: std.fs.Dir, path: []const u8) bool {
    return (dir.statFile(path) catch |err| {
        return err == error.IsDir;
    }).kind == .directory;
}

pub fn file_suffix_to_mime_type(file_type: []const u8) []const u8 {
    const eql = Strings.eql;
    const eq = struct {
        pub fn eq(a: []const u8, b: []const u8) bool {
            return eql(a, b);
        }
    }.eq;
    return if (eq(".jpg", file_type) or eq(".jpeg", file_type))
        "image/jpeg"
    else if (eq(".png", file_type))
        "image/png"
    else if (eq(".gif", file_type))
        "image/gif"
    else if (eq(".webp", file_type))
        "image/webp"
    else if (eq(".bmp", file_type))
        "image/bmp"
    else if (eq(".svg", file_type))
        "image/svg+xml"
    else if (eq(".mp4", file_type))
        "video/mp4"
    else if (eq(".h264", file_type))
        "video/H264"
    else if (eq(".h265", file_type))
        "video/H265"
    else if (eq(".mov", file_type))
        "video/quicktime"
    else if (eq(".webm", file_type))
        "video/webm"
    else if (eq(".mkv", file_type))
        "video/x-matroska"
    else if (eq(".3gp", file_type))
        "video/3gpp"
    else if (eq(".mpeg", file_type) or eq(".m4v", file_type))
        "video/mpeg"
    else if (eq(".avi", file_type))
        "video/x-msvideo"
    else if (eq(".wmv", file_type))
        "video/x-ms-wmv"
    else if (eq(".flv", file_type))
        "video/x-flv"
    else if (eq(".pdf", file_type))
        "application/pdf"
    else if (eq(".js", file_type))
        "application/javascript"
    else if (eq(".json", file_type))
        "application/json"
    else if (eq(".md", file_type))
        "text/markdown"
    else if (eq(".html", file_type) or eq(".htm", file_type))
        "text/html"
    else if (eq(".css", file_type))
        "text/css"
    else
        "text/plain";
}

const dummy = [0]u8{};

fn send_whole_file(client: *StreamServer.Connection, file: std.fs.File, content_type: []const u8, buffer: []u8) !void {
    try file.seekTo(0);

    const header = try Strings.format(buffer, HttpServer.TEMPLATE_RESPONSE, .{ "200 OK", content_type, (try file.stat()).size });

    const start = std.time.nanoTimestamp();
    const total_transfered: usize = (try file.stat()).size;
    // syscall
    _ = try std.os.sendfile(client.stream.handle, file.handle, 0, 0, &.{.{ .iov_base = header.ptr, .iov_len = header.len }}, &.{}, 0);

    const end = std.time.nanoTimestamp();
    const total_time_sec = @as(f64, @floatFromInt(end - start)) / 1_000_000_000.0;

    HttpServer.log_print(.Info, "Bandwidth: {d:.2}MB/s\n", .{@as(f64, @floatFromInt(total_transfered)) / 1000.0 / 1000.0 / total_time_sec});
}

test "shrunk_embed" {
    const e = @embedFile("directory_body_head.html");
    var f = shrink(@embedFile("directory_body_head.html"), false).val;
    std.debug.print("Shrunk: {any} => {any}\n", .{ @TypeOf(e), @TypeOf(f) });
    try std.testing.expect((&f).len < e.len);
    std.debug.print("Shrunk File(snippet): {s}\n", .{f[0..50]});
}
