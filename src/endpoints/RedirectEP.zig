const std = @import("std");
const DataTypes = @import("../data_types.zig");
const HTTP_CONFIG = DataTypes.HTTP_CONFIG;
const Request = DataTypes.Request;
const StreamServer = std.net.Server;
const IEndpoint = @import("../HttpServer.zig").IEndpoint;
const HttpServer = @import("../HttpServer.zig");
const Self = @This();
const Strings = DataTypes.Strings;

const TEMPLATE = DataTypes.ResponseBuilder
    .add_code("HTTP/1.1", "302 Found")
    .add_field("Server", "zhttp-server")
    .content_type("text/plain")
    .content_length("0")
    .location("{s}")
    .end();

pub fn template(buffer: []u8, location: []const u8) void {
    const ctype = "text/plain";
    const len = TEMPLATE.len - 3 - 3 + location.len + ctype.len;
    _ = len;
    const header = Strings.format(buffer, TEMPLATE, .{ ctype, location });
    _ = header;
}

path: *const fn (*Request) bool,
destination: []const u8,
endpoint: IEndpoint = .{ .name = "Redirect", .acceptFn = accept },

pub fn accepts(endpoint: *IEndpoint, req: *Request) bool {
    const self: *Self = @fieldParentPtr("endpoint", endpoint);
    return @call(.auto, self.path, .{req});
}

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    const self: *Self = @fieldParentPtr("endpoint", endpoint);
    if (!accepts(endpoint, ctx.req)) return false;
    const client = ctx.client;
    const req = ctx.req;
    _ = req;

    _ = client.stream.writer().print(TEMPLATE, .{self.destination}) catch unreachable;
    ctx.http_server.reaccept(client.*) catch unreachable;
    return true;
}
