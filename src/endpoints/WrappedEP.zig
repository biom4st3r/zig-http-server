const std = @import("std");
const HttpServer = @import("../HttpServer.zig");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.Server;
const Self = @This();

const Request = HttpServer.DataTypes.Request;

endpoint: IEndpoint = .{ .name = "WrappedEP", .acceptFn = accept },
wrapped: *IEndpoint,
acceptsFn: ?*const fn (*IEndpoint, *Request) bool,

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var self: *Self = @fieldParentPtr("endpoint", endpoint);
    const req = ctx.req;
    const client = ctx.client;
    _ = client;
    const a = if (self.acceptsFn) |af| @call(.auto, af, .{ self.wrapped, req }) else true;
    if (!a) return false;

    return self.wrapped.accept(ctx);
}
