const std = @import("std");
const HttpServer = @import("../HttpServer.zig");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.Server;
const Self = @This();

const Request = HttpServer.DataTypes.Request;

endpoint: IEndpoint = .{ .name = "WrappedEP", .acceptFn = accept },
wrapped: *IEndpoint,
host: []const u8,

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var self: *Self = @fieldParentPtr("endpoint", endpoint);
    if (std.mem.eql(ctx.req.get_header("Host") orelse return false, self.host)) return self.wrapped.accept(ctx);

    return false;
}
