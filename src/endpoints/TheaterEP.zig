const std = @import("std");
const DataTypes = @import("../data_types.zig");
const http_request = DataTypes.Request;
const StreamServer = std.net.Server;
const HttpServer = @import("../HttpServer.zig");
const IEndpoint = HttpServer.IEndpoint;
const Self = @This();
const Strings = DataTypes.Strings;

pub const Room = struct {
    current_time: f64 = 0.0,
    last_time_update: i64 = 0,
    paused: bool = true,
    src: []const u8 = "",
    pub fn getCurrentTime(self: *Room) f64 {
        if (self.last_time_update == 0) return 0.0;
        if (self.paused) return self.current_time;
        var delta = std.time.milliTimestamp() - self.last_time_update;
        return self.current_time + @as(f64, @floatFromInt(delta)) / 1000.0;
    }
};

rooms: [50]Room = [_]Room{Room{}} ** 50,
endpoint: IEndpoint = .{ .name = "Theater", .acceptFn = accept },
alloc: std.mem.Allocator,
mutex: std.Thread.Mutex,
route: []const u8,

pub fn accepts(endpoint: *IEndpoint, req: *http_request) bool {
    var self: *Self = @fieldParentPtr("endpoint", endpoint);
    return Strings.starts_with(self.route, req.path[1..]);
}

const FMT =
    \\{{"paused": {any}, "currentTime": {d:.2}, "src": "{s}"}}
;

const TEMPLATE = &@import("../comptime_shrink.zig").shrink_embed(@embedFile("theater_template.html"), true).val;

const Settings = enum {
    currentTime,
    paused,
    src,
};

pub fn post(self: *Self, req: *http_request, client: *StreamServer.Connection, room: *Room) !void {
    var split = Strings.splitReverse(req.full_request, "\r\n");
    if (split.next()) |change| {
        std.debug.print("{s}\n", .{change});
        var csplit = Strings.split(change, "=");
        if (csplit.next()) |key| {
            var value = csplit.next();
            if (value == null) return;
            self.mutex.lock();
            defer self.mutex.unlock();
            switch (std.meta.stringToEnum(Settings, key) orelse {
                std.debug.print("\nInvalid argumnent: {s}\n\n", .{key});
                return;
            }) {
                .currentTime => {
                    room.current_time = std.fmt.parseFloat(f64, value.?) catch unreachable;
                    room.last_time_update = std.time.milliTimestamp();
                },
                .paused => {
                    room.paused = Strings.eql(value.?, "true");
                },
                .src => {
                    self.alloc.free(room.src);
                    room.src = self.alloc.dupe(u8, value.?) catch "https://google.com/";
                    // free room.src
                    // alloc new src from csplit.next()
                },
            }
        }
    }
    std.debug.print("{any}\n", .{room});
    try HttpServer.send_simple_response(client, "200 OK", "text/plain", "");
}

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var self: *Self = @fieldParentPtr(Self, "endpoint", endpoint);
    if (!accepts(endpoint, ctx.req)) return false;
    const client = ctx.client;
    const req = ctx.req;
    defer client.stream.close();

    var room_id: u16 = std.fmt.parseInt(u16, req.path[self.route.len + 1 ..], 0) catch return true;
    var room = &self.rooms[room_id % self.rooms.len];
    var buffer: [512]u8 = undefined;
    switch (req.method orelse return true) {
        .OPTIONS => { // Tell them about the room
            self.mutex.lock();
            defer self.mutex.unlock();
            var json = Strings.format(&buffer, FMT, .{ room.paused, room.getCurrentTime(), room.src }) catch unreachable;
            HttpServer.send_simple_response(client, "200 OK", "application/json", json) catch return true;
        },
        .POST => { // client makes changes to the room
            post(self, req, client, room) catch unreachable;
        },
        .GET => { // client gets html for the room
            std.debug.print("gET?\n\n", .{});
            HttpServer.send_simple_response(client, "200 OK", "text/html", TEMPLATE) catch return true;
        },
        else => { // 7_(',')_F
            HttpServer.send_simple_response(client, "404 Not FOund", "text/plain", "") catch return true;
        },
    }
    return true;
}
