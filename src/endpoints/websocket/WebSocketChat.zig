const std = @import("std");
const WSEndpoint = @import("WebSocket.zig");

pub const ChatOps = enum(u8) {
    full = 0,
    msg = 1,
};

pub var sockets: [50]?*WSEndpoint.WebSocket = .{null} ** 50;
pub var mutex: std.Thread.Mutex = .{};
pub var clients: usize = 0;

pub fn web_socket_chat(ws: *WSEndpoint.WebSocket) void {
    std.debug.print("hello0?\n", .{});
    mutex.lock();
    defer mutex.unlock();
    for (&sockets, 0..) |*socket, i| {
        mutex.unlock();
        std.debug.print("{d}\n", .{i});
        defer mutex.lock();
        if (socket.*) |_| {} else {
            clients += 1;
            std.debug.print("added to chat\n", .{});
            socket.* = ws;
            // Make non-block
            // TODO Use epoll
            var flag = std.os.fcntl(ws.socket.stream.handle, std.os.F.GETFL, 0) catch unreachable;
            flag |= std.os.SOCK.NONBLOCK;
            _ = std.os.fcntl(ws.socket.stream.handle, std.os.F.SETFL, flag) catch unreachable;
            return;
        }
    }
    ws.send(.text_frame, null, &[_]u8{@intFromEnum(ChatOps.full)}) catch {};
    ws.close();
}

pub fn do_op(ws: *WSEndpoint.WebSocket, op: ChatOps, data: []const u8) void {
    _ = ws;
    switch (op) {
        .msg => {
            for (&sockets) |*socket| if (socket.*) |client| {
                client.send(.text_frame, null, data) catch {
                    std.debug.print("Error sending to client\n", .{});
                    remove_client(socket);
                };
            };
        },
        else => {},
    }
}

fn remove_client(socket: *?*WSEndpoint.WebSocket) void {
    std.debug.print("Removing client\n", .{});
    clients -= 1;
    mutex.lock();
    defer mutex.unlock();
    socket.*.?.close();
    socket.* = null;
}

pub fn worker() void {
    while (true) {
        defer std.time.sleep(10_000_000); // 10ms
        mutex.lock();
        defer mutex.unlock();
        if (clients == 0) continue;
        for (&sockets, 0..) |*socket, i| {
            _ = i;
            if (socket.*) |client| {
                mutex.unlock();
                defer mutex.lock();
                if (client.recv() catch |err| switch (err) {
                    error.WouldBlock => {
                        continue;
                    },
                    else => {
                        std.debug.print("error on recv: {?}\n", .{err});
                        remove_client(socket);
                        continue;
                    },
                }) |frame| {
                    frame.print();
                    if (frame.payload.len < 2) continue;
                    const op = std.meta.intToEnum(ChatOps, frame.payload[0]) catch {
                        std.debug.print("error std.meta.intToEnum(ChatOps\n", .{});
                        remove_client(socket);
                        continue;
                    };
                    do_op(client, op, frame.payload);
                }
            }
        }
    }
}
