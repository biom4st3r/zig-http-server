const std = @import("std");
const StreamServer = std.net.Server;
const DataTypes = @import("../../data_types.zig");
const HttpServer = @import("../../HttpServer.zig");
const IEndpoint = HttpServer.IEndpoint;
const Request = DataTypes.Request;
const Self = @This();
const Strings = DataTypes.Strings;

fn correct_big_struct(comptime T: type, t: *T) void {
    inline for (std.meta.fields(T)) |field| {
        switch (@typeInfo(field.type)) {
            .Bool => {},
            .Enum => {
                @field(t, field.name) = @as(field.type, @enumFromInt(@bitReverse(@intFromEnum(@field(t, field.name)))));
            },
            .Int => |i| {
                if (i.bits != 1) {
                    @field(t, field.name) = @bitReverse(@field(t, field.name));
                }
            },
            .Struct => {
                correct_big_struct(field.type, @field(t, field.name));
            },
            else => |typeinfo| {
                @compileLog("unhandled type: ", typeinfo);
            },
        }
    }
}

pub fn read_big_struct(comptime T: type, reader: anytype) !T {
    const size = @sizeOf(T);
    var buffer: [size]u8 = undefined;
    if (try reader.read(&buffer) != buffer.len) {
        return error.UnexpectedError;
    }

    for (&buffer) |*char| {
        char.* = @bitReverse(char.*);
    }

    const t: *T = @as(*T, @ptrCast(@alignCast(&buffer)));
    correct_big_struct(T, t);
    return t.*;
}

pub fn to_big_struct_bytes(comptime T: type, t: T) [@sizeOf(T)]u8 {
    const size = @sizeOf(T);
    var host: T = t;

    const buffer: *[size]u8 = @ptrCast(@alignCast(&host));
    correct_big_struct(T, &host);
    for (buffer) |*char| {
        char.* = @bitReverse(char.*);
    }
    return buffer.*;
}

pub const WebSocket = struct {
    pub const Base64 = std.base64.standard.Encoder;
    pub const Sha = std.crypto.hash.Sha1;
    __host: std.mem.Allocator, // Because I cannot get Arena to host the Object
    arena: std.heap.ArenaAllocator,
    allocator: std.mem.Allocator,
    version: u16,
    client_key: []const u8,
    socket: StreamServer.Connection,
    recv_buffer: ?[]u8 = null,

    /// For some reason after this function socket.arena is not long valid memory and segfaults when trying to allocate
    pub fn wtfinit(client: StreamServer.Connection, req: *Request, allocator: std.mem.Allocator) !*WebSocket {
        var arena = std.heap.ArenaAllocator.init(allocator);
        var alloc = arena.allocator();
        const ws: *WebSocket = try allocator.create(WebSocket);
        ws.* = .{
            .__host = allocator,
            .arena = arena,
            .allocator = alloc,
            .version = std.fmt.parseInt(u16, req.get_header("Sec-WebSocket-Version") orelse "0", 0) catch 0,
            .client_key = try alloc.dupe(req.get_header("Sec-WebSocket-Key") orelse return error.MissingWebSocketKey),
            .socket = client,
        };
        if (false) return error.noerror;
        return ws;
    }

    pub fn init(self: *WebSocket, client: StreamServer.Connection, req: *Request, allocator: std.mem.Allocator) !void {
        self.arena = std.heap.ArenaAllocator.init(allocator);
        errdefer self.arena.deinit();
        self.allocator = self.arena.allocator();
        self.* = .{
            .__host = allocator,
            .arena = self.arena,
            .allocator = self.allocator,
            .version = std.fmt.parseInt(u16, req.get_header("Sec-WebSocket-Version") orelse "0", 0) catch 0,
            .client_key = try self.allocator.dupe(req.get_header("Sec-WebSocket-Key") orelse return error.MissingClientKey),
            .socket = client,
            // .protocols = req.get_header("Sec-WebSocket-Protocol");
        };
    }

    pub fn ping(self: *WebSocket) !?Frame {
        try self.send(.ping, null, "");
        // var frame: ?Frame = ;
        return try self.recv();
        // if (frame) |f| f.print() else std.debug.print("No Data Received\n", .{});
        // return
    }

    pub fn close(self: *WebSocket) void {
        self.socket.stream.close();
        self.arena.deinit();
        self.__host.destroy(self);
    }

    pub fn create_server_key(self: *WebSocket, server_key: []u8) []const u8 {
        var hash: [Sha.digest_length]u8 = undefined;
        var sha = std.crypto.hash.Sha1.init(.{});
        sha.update(self.client_key);
        sha.update(rfc6455_magic_guid);
        sha.final(&hash);

        return std.base64.standard.Encoder.encode(server_key, &hash);
    }

    pub fn send(self: *WebSocket, opcode: Frame.Opcode, mask: ?u32, data: []const u8) !void {
        var header: Frame.Header = .{
            .fin = true,
            .opcode = opcode,
            .masked = if (mask) |_| true else false,
            .len = 0,
        };
        if (data.len < 126) {
            header.len = @intCast(data.len & 0b0111_1111);
        } else if (data.len <= 0xFFFF) {
            header.len = 126;
        } else header.len = 127;

        var writer = self.socket.stream.writer();
        var b_header = to_big_struct_bytes(Frame.Header, header);
        // header.fix_outgoing();
        // try writer.writeIntLittle(u16, @as(*u16, @ptrCast(&header)).*);
        _ = try writer.write(&b_header);
        switch (header.len) {
            126 => {
                try writer.writeIntBig(u16, @as(u16, @intCast(data.len & 0xFFFF)));
            },
            127 => {
                try writer.writeIntBig(u64, data.len);
            },
            else => {},
        }
        if (mask) |_| {
            return error.NotImplemented;
        }
        _ = try writer.write(data);
    }

    pub fn recv(self: *WebSocket) !?Frame {
        var buffer = if (self.recv_buffer) |b| b else blk: {
            @setCold(true);
            self.recv_buffer = try self.allocator.alloc(u8, 4096);
            break :blk self.recv_buffer.?;
        };
        const header = try read_big_struct(Frame.Header, self.socket.stream.reader());
        const len = try self.socket.stream.read(buffer);

        // const len = try self.socket.stream.read(buffer);

        if (len == buffer.len) {
            // use the arena
            return error.FrameToLarge;
        }

        const frame: ?Frame = Frame.parse_frame(header, buffer[0..len]);
        // var frame: ?Frame = Frame.parse_frame(buffer[0..len]);
        if (frame) |f| {
            if (f.is_fragment()) {
                return error.NotImplemented;
            }
        }
        return frame;
    }
};

endpoint: IEndpoint = .{ .name = "WebSocket", .acceptFn = accept },
options: struct {
    protocol: ?[]const u8 = null,
    path: ?[]const u8 = null,
    websocket_handle: ?*const fn (*WebSocket) void,
},

pub fn accepts(endpoint: *IEndpoint, req: *Request) bool {
    const self: *Self = @fieldParentPtr("endpoint", endpoint);

    const wants_upgrade = std.ascii.eqlIgnoreCase(req.get_header("Upgrade: ") orelse return false, "websocket");
    // std.debug.print("wants_upgrade: {?}\n", .{wants_upgrade});
    if (wants_upgrade) {
        const paths_match = if (self.options.path) |p| Strings.eql(p, req.path) else true;
        // std.debug.print("paths_match: {?}\n", .{paths_match});
        if (paths_match) {
            const protocol_match = if (self.options.protocol) |p| Strings.contains(req.get_header("Sec-WebSocket-Protocol") orelse "", p) else true;
            // std.debug.print("protocol_match: {?}\n", .{protocol_match});
            return protocol_match;
        }
    }
    return false;
}

pub const Frame = struct {
    pub const Opcode = enum(u4) {
        continued_frame = 0,
        text_frame = 1,
        binary_frame = 2,
        connection_close = 8,
        ping = 9,
        pong = 10,
        pub fn validate(op: Opcode) ?Opcode {
            return switch (@intFromEnum(op)) {
                @intFromEnum(Opcode.continued_frame) => op,
                @intFromEnum(Opcode.text_frame) => op,
                @intFromEnum(Opcode.binary_frame) => op,
                @intFromEnum(Opcode.connection_close) => op,
                @intFromEnum(Opcode.ping) => op,
                @intFromEnum(Opcode.pong) => op,
                else => null,
            };
        }
    };
    pub const Header = packed struct(u16) {
        fin: bool = false,
        compressed: bool = false,
        rsv2: u1 = 0,
        rsv3: u1 = 0,
        opcode: Opcode,
        masked: bool = false,
        len: u7 = 0,
    };

    header: Header,
    len: u64,
    mask: ?u32,
    payload: []u8,
    pub fn is_fragment(self: *const Frame) bool {
        return !self.header.fin;
    }

    pub fn unmask(self: *Frame) void {
        const LENGTH = 32;
        // Make the type
        const Vector: type = @Vector(LENGTH, u8);

        const key: *[4]u8 = @ptrCast(&(self.mask.?));
        // Build a reuseable Vector by repeating the key
        const MASK: Vector = blk: {
            var temp: [32]u8 = undefined;
            for (0..LENGTH) |i| {
                temp[i] = key[i % 4];
            }
            break :blk temp;
        };

        // unmask bulk of the data
        var remaining = self.payload;
        while (remaining.len > LENGTH) : (remaining = remaining[0..LENGTH]) {
            var data: Vector = remaining[0..LENGTH].*;
            data = data ^ MASK;
            @memcpy(remaining[0..LENGTH], &@as([32]u8, data));
        }
        // unmask manually for the tail < 32len
        for (remaining, 0..) |*char, i| {
            char.* = char.* ^ key[i % 4];
        }
    }

    pub fn print(self: *const Frame) void {
        const enumm: @TypeOf(HttpServer.LOGGING) = .Info;
        if (@intFromEnum(HttpServer.LOGGING) >= @intFromEnum(enumm)) {
            const op = Frame.Opcode.validate(self.header.opcode);
            if (op == null) {
                std.debug.print("illegal: {X}\n", .{@intFromEnum(self.header.opcode)});
            }
            std.debug.print("WSFrame: \n|fin: {?}, compressed: {?}, rsv2: {d}, rsv3: {d}, masked: {?}, op: {s}|\n|len: {d}|\n|mask: {?}|\n||{s}\n", .{
                self.header.fin,
                self.header.compressed,
                self.header.rsv2,
                self.header.rsv3,
                self.header.masked,
                if (op) |o| @tagName(o) else "illegal",
                self.len,
                self.mask,
                self.payload,
            });
        }
    }

    pub fn parse_frame(header: Header, data: []u8) ?Frame {
        if (header.opcode.validate() == null) return null;
        var frame: Frame = .{
            .header = header,
            .len = 0,
            .mask = null,
            .payload = undefined,
        };

        var mask_start: u8 = 0;
        switch (frame.header.len) {
            // If 126, the following 2 bytes interpreted as a 16-bit unsigned integer are the payload length
            126 => {
                frame.len = std.mem.bytesAsValue(u16, data[0..2]).*;
                // if (true) @compileError("heloo?126");
                mask_start = 2;
            },
            // If 127, the following 8 bytes interpreted as a 64-bit unsigned integer
            127 => {
                // if (true) @compileError("heloo?127");
                frame.len = std.mem.bytesAsValue(u64, data[0..8]).*;
                mask_start = 8;
            },
            else => {
                frame.len = frame.header.len;
                mask_start = 0;
            },
        }

        if (frame.header.masked) {
            frame.mask = std.mem.bytesAsValue(u32, @as(*[4]u8, @ptrCast(data[mask_start .. mask_start + 4]))).*;
            frame.payload = data[mask_start + 4 ..];
            frame.unmask();
        } else {
            frame.mask = null;
            frame.payload = data[mask_start..];
        }
        return frame;
    }
};

pub const rfc6455_magic_guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

pub const Template = DataTypes.ResponseBuilder
    .add_code("HTTP/1.1", "101 Switching Protocols")
    .add_field("Upgrade", "websocket")
    .add_field("Connection", "Upgrade")
    .add_field("Sec-WebSocket-Accept", "{s}");

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    const self: *Self = @fieldParentPtr("endpoint", endpoint);
    if (!accepts(endpoint, ctx.req)) return false;
    const client = ctx.client;
    const req = ctx.req;
    var ws: *WebSocket = ctx.http_server.allocator.create(WebSocket) catch unreachable;

    ws.init(client.*, req, ctx.http_server.allocator) catch unreachable;
    // var socket = WebSocket.wtfinit(client.*, req, endpoint.http_server.allocator) catch unreachable;

    var _server_key: [40]u8 = undefined;
    const server_key = ws.create_server_key(&_server_key);
    if (self.options.protocol) |protocol| {
        const WProtocol = comptime Template.add_field("Sec-WebSocket-Protocol", "{s}").end();
        _ = client.stream.writer().print(WProtocol, .{ server_key, protocol }) catch unreachable;
    } else {
        const NoProtocol = comptime Template.end();
        _ = client.stream.writer().print(NoProtocol, .{server_key}) catch unreachable;
    }
    if (self.options.websocket_handle) |handle| {
        @call(.auto, handle, .{ws});
    } else {
        if (ws.ping() catch unreachable) |frame| frame.print();
        std.debug.print("You must register a function to WebSocket{{.options{{.websocket_handle}}}} if you want to do anything useful with the websockets.\n", .{});
        ws.close();
    }
    // socket.close();
    return true;
}
