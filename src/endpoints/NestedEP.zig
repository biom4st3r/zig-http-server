const std = @import("std");
const HttpServer = @import("../HttpServer.zig");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.Server;
const Self = @This();

const Request = HttpServer.DataTypes.Request;

endpoint: IEndpoint = .{ .name = "NestedEP", .acceptFn = accept },
endpoints: std.ArrayList(*IEndpoint),

pub fn init(alloc: std.mem.Allocator) @This() {
    return .{
        .endpoints = std.ArrayList(*IEndpoint).init(alloc),
    };
}

pub fn register(self: *@This(), endpoint: *IEndpoint) void {
    self.endpoints.append(endpoint) catch unreachable;
}

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    const self: *Self = @fieldParentPtr("endpoint", endpoint);
    for (self.endpoints.items) |ep| {
        if (ep.accept(ctx)) return true;
    }
    return false;
}
