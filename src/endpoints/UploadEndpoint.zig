const std = @import("std");
const DataTypes = @import("../data_types.zig");
const HTTP_CONFIG = DataTypes.HTTP_CONFIG;
const http_request = DataTypes.Request;
const StreamServer = std.net.StreamServer;
const HttpServer = @import("../HttpServer.zig");
const IEndpoint = HttpServer.IEndpoint;
const Self = @This();
const Strings = DataTypes.Strings;

endpoint: IEndpoint = .{ .name = "UploaderEP", .acceptFn = accept },

pub fn accepts(endpoint: *IEndpoint, req: *http_request) bool {
    _ = endpoint;
    return req.method == .POST;
}

fn upload_failed(client: *StreamServer.Connection, buffer: []u8, err: []const u8) void {
    const buff = HttpServer.create_simple_response(buffer, "5XX Upload Failed", "text/plain", err) catch return;

    if (buff) |data| {
        _ = client.stream.write(data) catch return;
    }
}

pub fn RollingBuffer(comptime size: usize) type {
    const half = @divFloor(size, 2);
    return struct {
        big_buffer: [size]u8 = undefined,
        nexti: u1 = 0,
        primed: usize = 0,
        pub fn next(self: *@This()) []u8 {
            const lb = 0 + (half * self.nexti);
            const ub = half + (half * self.nexti);
            self.nexti = @addWithOverflow(self.nexti, 1)[0];
            self.primed += 1;
            return self.big_buffer[lb..ub];
        }

        pub fn current(self: *@This()) []u8 { // opposite of next
            const currenti = @subWithOverflow(self.nexti, 1)[0];
            const lb = 0 + (half * currenti);
            const ub = half + (half * currenti);
            return self.big_buffer[lb..ub];

            // var b = if (self.nextb) &self.buffer1 else &self.buffer0;
            // return b;
        }

        pub fn prev(self: *@This()) []u8 {
            const lb = 0 + (half * self.nexti);
            const ub = half + (half * self.nexti);
            return self.big_buffer[lb..ub];

            // var b = if (self.nextb) &self.buffer0 else &self.buffer1;
            // return b;
        }

        inline fn unprime(self: *@This()) void {
            self.primed = 0;
        }

        pub fn contains(self: *@This(), comptime search_area_size: usize, needle: []const u8) bool {
            if (self.primed < 2) {
                return Strings.contains(self.current()[self.current().len - search_area_size ..], needle);
            }
            var buffer: [search_area_size * 2]u8 = undefined;
            // When primed this is always the correct order.
            @memcpy(buffer[0..search_area_size], self.prev()[self.prev().len - search_area_size ..]);
            @memcpy(buffer[search_area_size..], self.current()[0..search_area_size]);
            // std.debug.print("\n\n{s}\n\n", .{&buffer});
            return Strings.contains(&buffer, needle);
        }
    };
}

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    const self: *Self = @fieldParentPtr("endpoint", endpoint);
    _ = self;
    if (!accepts(endpoint, ctx.req)) return false;
    const client = ctx.client;
    const req = ctx.req;
    defer client.stream.close();
    var packet_buffer: [256]u8 = undefined;
    bulk_upload(endpoint, req, client, &packet_buffer) catch |err| {
        upload_failed(client, &packet_buffer, @errorName(err));
    };
    if (HttpServer.create_simple_response(&packet_buffer, "200 OK", "text/plain", "Success") catch unreachable) |data| {
        _ = client.stream.write(data) catch {
            HttpServer.log_print(.Err, "Failed final response\n", .{});
        };
    }
    return true;
}

pub const DEBUG = false;

pub fn bulk_upload(endpoint: *IEndpoint, req: *http_request, client: *StreamServer.Connection, packet_buffer: []u8) !void {
    _ = endpoint;
    defer {
        HttpServer.log_print(.All, "End of upload.\n", .{});
    }
    var rbuffer = RollingBuffer(4096){};
    // var self = @fieldParentPtr(Self, "endpoint", endpoint); // Zig magic

    const boundry: []const u8 = (req.find_segment("boundary=", "\r\n") orelse {
        HttpServer.log_print(.Info, "bad upload. no boundry\ncontent: {s}\n", .{req.full_request});
        return error.MissingBoundry;
    })["boundary=".len..];
    const file_name = (req.find_segment("filename=\"", "\"\r\n") orelse {
        HttpServer.log_print(.Info, "bad upload. missing file_name: {s}\n", .{req.full_request});
        return error.MissingFileName;
    })["filename=\"".len..];
    var file_meta = try Strings.format(packet_buffer, "{s}.{s}", .{ file_name, "metadata" }); //req.path
    // TODO save metadata to disk. Req and ip.
    const file_out = file_meta[0 .. file_meta.len - "metadata".len];

    // HttpServer.log_print(.All, "\nout: {s}\n", .{file_out});
    HttpServer.log_print(.All, "Creating file {s}\n", .{file_out});
    var FILE = std.fs.cwd().createFile(file_out, .{}) catch |err| {
        HttpServer.log_print(.Info, "failed upload: {any} @ {s} {s}", .{ err, req.path, file_out });
        if (HttpServer.create_simple_response(packet_buffer, "403 Forbidden", "", "") catch null) |data| {
            _ = try client.stream.write(data);
        }
        return;
    };
    defer FILE.close();

    var DEBUG_FILE: if (DEBUG) std.fs.File else void = if (DEBUG) std.fs.cwd().createFile("recvd_data.log", .{}) catch unreachable else {};
    defer {
        if (DEBUG) DEBUG_FILE.close();
    }
    // std.debug.print("len: {d} boundry: |{s}|\n", .{ boundry.len, boundry });
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
    // const boundry_len = boundry.len + 2 + 2; // prepend with -- and/or append if it's the tail
    const content_length: u64 = blk: {
        var field = req.find_segment("\nContent-Length: ", "\r\n") orelse return {
            HttpServer.log_print(.Info, "bad upload. no Content-Length\ncontent: {s}\n", .{req.full_request});
        };
        // std.debug.print("Content-Length: {s}\n", .{field["\nContent-Length: ".len..]});
        break :blk try std.fmt.parseInt(u64, field["\nContent-Length: ".len..], 10);
    };

    const START = std.time.microTimestamp();
    defer {
        const DURATION = std.time.microTimestamp() - START;
        const seconds: f32 = @as(f32, @floatFromInt(DURATION)) / 1_000_000.0;
        HttpServer.log_print(.All, "Avg Bandwidth: {d:.2} MB/s", .{(@as(f32, @floatFromInt(content_length)) / 1_000_000.0) / seconds});
    }
    var content_received: u64 = req.full_request.len;

    var split = Strings.splitReverse(req.full_request, "\r\n\r\n");
    if (split.next()) |content| {
        _ = try FILE.write(content);
    }
    if (DEBUG) _ = DEBUG_FILE.write(req.full_request) catch unreachable;
    var last = std.time.milliTimestamp();
    while (true) {
        var size = try client.stream.read(rbuffer.next());
        while (size == rbuffer.current().len) : (size = try client.stream.read(rbuffer.next())) {
            if (DEBUG) _ = DEBUG_FILE.write(rbuffer.current()) catch unreachable;
            if (rbuffer.contains(70, boundry)) { // data+boundry fit perfectly in buffer
                HttpServer.log_print(.All, "\ncontent0: |{s}|\n", .{rbuffer.current()[0..size]});
                unreachable; // TODO
            } else { // only data
                _ = try FILE.write(rbuffer.current());
                content_received += size;
            }
        }
        if (DEBUG) _ = DEBUG_FILE.write(rbuffer.current()) catch unreachable;
        content_received += size;

        if (size >= boundry.len + 2 + 2) {
            var content = rbuffer.current()[0..size];
            const index = Strings.find(boundry, content[content.len - (boundry.len + 2 + 2) ..]);
            if (index) |_| { // has boundry
                content = content[0 .. content.len - "\r\n".len - "--".len - boundry.len - "--".len - "\r\n".len];
                _ = try FILE.write(content);
                HttpServer.log_print(.All, "exit0\n", .{});
                return; // Done?
            } else { // normal data
                _ = try FILE.write(content);
            }
        } else if (rbuffer.contains(70, boundry)) { //segmented buffer
            // TODO Truncate

            HttpServer.log_print(.All, "exit1\n", .{});
            return;
        } else if (size == 0) {
            HttpServer.log_print(.All, "exit2", .{});
            return;
        } else { // just data and a half filled buffer.
            _ = try FILE.write(rbuffer.current()[0..size]); // rbuffer is in an illegal state with the wrong data at the tail of current
            rbuffer.unprime(); // back to a legal state.
        }
        if (std.time.milliTimestamp() - last > 250) {
            last = std.time.milliTimestamp();

            var pb: [20]u8 = undefined;
            const p_buffer = try Strings.format(&pb, "Progress: {d:.2}", .{(@as(f32, @floatFromInt(content_received)) / @as(f32, @floatFromInt(content_length))) * 100.0});
            HttpServer.log_print(.Info, "{s}%", .{p_buffer});
            HttpServer.log_print(.Info, "\n", .{});
            // if (HttpServer.create_simple_response(packet_buffer, "200 OK", "text/plain", p_buffer) catch null) |data| {
            //     // std.debug.print("3\n", .{});
            //     _ = try client.stream.write(data);
            //     // std.debug.print("4\n", .{});
            // } else {
            //     std.debug.print("Couldn't fill buffer to send!!!\n", .{});
            //     return;
            // }
        }
    }
}
