const std = @import("std");
const DataTypes = @import("data_types.zig");
const Strings = DataTypes.Strings;

pub const HTTP_CONFIG = struct {
    root_path: []const u8,
    ip: []const u8,
    port: u16,
    threads: usize,
    gen_port: bool,
    reuse_port: bool,
    logging: u8,
    log_file: ?[]const u8,
    pub fn set_root_path(self: *HTTP_CONFIG, path: []const u8) void {
        if (path[path.len - 1] == '/') {
            self.root_path = path[0 .. path.len - 1];
        } else {
            self.root_path = path;
        }
    }
    pub fn init(self: *@This(), alloc: std.mem.Allocator) !void {
        self.* = .{
            .root_path = try std.fs.cwd().realpathAlloc(alloc, ".") catch unreachable,
            .port = 8080,
            .ip = "0.0.0.0",
            .threads = @intCast(std.Thread.getCpuCount() catch unreachable),
            .gen_port = false,
            .reuse_port = false,
            .logging = 0,
            .log_file = null,
        };
    }
};

const help =
    \\  Simple http server
    \\  zhttp-server --root=/home/usr
    \\
    \\    -h               This menu
    \\    --h
    \\    --help
    \\    --root            Set the root path[.]
    \\    --ip              Listening ip[0.0.0.0]
    \\    --port=[#|gen]    Listening root[8080] or gen to find the first
    \\                         free port after 8080
    \\    --reuse_port      Allows port reuse[false]
    \\    --threads         Threads to be created to serve files[system cores]
    \\    --logging         The level of logging to do[]{{range:v-vvvv}}
    \\    --log_file        Path to a file to write logs to instead of printing
    \\
    \\
;

pub const Args = enum {
    @"-h",
    @"--h",
    @"--help",
    @"--root",
    @"--ip",
    @"--port",
    @"--threads",
    @"--gen_port",
    @"--reuse_port",
    @"--logging",
    @"--log_file",
};

const builtin = @import("builtin");

pub fn parseTildeOwned(allocator: std.mem.Allocator, path: []const u8) ?[]const u8 {
    if (path[0] != '~') {
        return null;
    }

    const home = std.process.getEnvVarOwned(allocator, "HOME") catch unreachable;
    defer allocator.free(home);
    var parsed = allocator.alloc(u8, home.len + path.len - 1) catch unreachable;
    @memcpy(parsed[0..home.len], home);
    // std.debug.print("{d} {d}", .{ parsed[home.len..].len, path.len });
    @memcpy(parsed[home.len..], path[1..]);
    return parsed;
}

pub fn parse(allocator: std.mem.Allocator) HTTP_CONFIG {
    var config = HTTP_CONFIG{
        .root_path = std.fs.cwd().realpathAlloc(allocator, ".") catch unreachable,
        .port = 8080,
        .ip = "0.0.0.0",
        .threads = @min(4, @as(u64, @intCast(std.Thread.getCpuCount() catch unreachable))),
        .gen_port = false,
        .reuse_port = false,
        .logging = 0,
        .log_file = null,
    };
    var has_set_path = false;

    var iter = if (builtin.os.tag == .windows) std.process.argsWithAllocator(allocator) catch unreachable else std.process.args();
    _ = iter.next();
    while (iter.next()) |arg| {
        const split = std.mem.indexOf(u8, arg, "=") orelse {
            std.debug.print("\nInvalid argumnent: Missing '=' {s}\n\n" ++ help, .{arg});
            std.posix.exit(0);
        };
        switch (std.meta.stringToEnum(Args, arg[0..split]) orelse {
            std.debug.print("\nInvalid argumnent: {s}\n\n" ++ help, .{arg});
            std.posix.exit(0);
        }) {
            .@"-h", .@"--help", .@"--h" => {
                std.debug.print(help, .{});
                std.posix.exit(0);
            },
            .@"--root" => {
                if (!has_set_path) {
                    allocator.free(config.root_path);
                }
                has_set_path = true;
                var path: []const u8 = undefined;
                if (parseTildeOwned(allocator, arg[split + 1 ..])) |_path|
                    path = _path
                else
                    path = arg[split + 1 ..];

                config.root_path = path;
                config.set_root_path(config.root_path);
            },
            .@"--ip" => {
                config.ip = arg[split + 1 ..];
            },
            .@"--port" => {
                if (Strings.starts_with("=gen", arg[split..])) {
                    config.gen_port = true;
                    continue;
                }
                config.port = std.fmt.parseUnsigned(u16, arg[split + 1 ..], 0) catch {
                    std.debug.print("Bad port\n", .{});
                    std.posix.exit(0);
                };
            },
            .@"--threads" => {
                config.threads = std.fmt.parseUnsigned(u16, arg[split + 1 ..], 0) catch {
                    std.debug.print("Bad Thread count\n", .{});
                    std.posix.exit(0);
                };
            },
            .@"--gen_port" => {
                config.gen_port = true;
            },
            .@"--reuse_port" => {
                config.reuse_port = true;
            },
            .@"--logging" => {
                const lvl = arg[split + 1 ..].len & 0xFF;
                config.logging = @intCast(lvl);
            },
            .@"--log_file" => {
                config.log_file = arg[split + 1 ..]; //std.fs.realpathAlloc(allocator, arg[split + 1 ..]) catch unreachable;
            },
        }
    }
    if (config.threads == 0) {
        std.debug.print("\nWhat did you think would happen?\n", .{});
        std.posix.exit(0);
    }
    return config;
}
