const std = @import("std");
const DataTypes = @import("data_types.zig");
const Args = @import("args.zig");
const parse = @import("args.zig").parse;
const HTTP_CONFIG = Args.HTTP_CONFIG;
const WSEndpoint = @import("endpoints/websocket/WebSocket.zig");
const HttpServer = @import("HttpServer.zig");
const build_info = @import("build_info");
const StreamServer = std.net.Server;
const Queue = std.DoublyLinkedList(std.net.Server.Connection);
const Threading = @This();

threads: []std.Thread,
server: *HttpServer,
main_queue: struct {
    queue: Queue = Queue{},
    mutex: std.Thread.Mutex = .{},
    condition: std.Thread.Condition = .{},
    alloc: std.mem.Allocator,
    pub fn push(self: *@This(), client: StreamServer.Connection) !void {
        var node = try self.alloc.create(Queue.Node);
        node.data = client;
        self.push_node(node);
    }
    pub fn push_node(self: *@This(), node: *Queue.Node) void {
        self.mutex.lock();
        self.queue.append(node);
        self.mutex.unlock();
        self.condition.signal();
    }
},

pub fn append(self: *Threading, client: StreamServer.Connection) !void {
    if (self.threads.len != 0) {
        try self.main_queue.push(client);
    } else {
        HttpServer.mainloop_noerr(self.server, client);
    }
}
pub fn resubmit(self: *Threading, client: StreamServer.Connection) !void {
    if (self.threads.len == 0) {
        client.stream.close();
    } else {
        try self.append(client);
    }
}

const POLLIN = if (@import("builtin").os.tag == .windows)
    std.os.windows.ws2_32.POLL.RDNORM | std.os.windows.ws2_32.POLL.RDBAND
else
    std.posix.system.POLL.IN;
const POLLFD = if (@import("builtin").os.tag == .windows) std.os.windows.ws2_32.pollfd else std.os.linux.pollfd;
const poll = if (@import("builtin").os.tag == .windows) std.os.windows.poll else std.os.linux.poll;

pub const Work = struct {
    fn work(self: *Threading) void {
        // ?? Why would this be slower than single_check????
        const SIZE = 50; // TODO Vary based on load
        var nodes: [SIZE]*Queue.Node = undefined;
        var pollfd: [SIZE]POLLFD = undefined;

        while (true) {
            multi_check(self, &nodes, &pollfd);
        }
    }
    pub fn single_check(self: *Threading) void {
        var m = &self.main_queue.mutex;
        m.lock();
        defer m.unlock();
        while (self.main_queue.queue.popFirst()) |node| {
            m.unlock();
            defer m.lock();
            if (data_waiting(node.data) catch |err| {
                std.debug.print("{any}\n", .{err});
                node.data.stream.close();
                self.main_queue.alloc.destroy(node);
                continue;
            }) {
                HttpServer.mainloop_noerr(self.server, node.data);
                self.main_queue.alloc.destroy(node);
            } else {
                self.main_queue.push_node(node);
            }
        }
        self.main_queue.condition.wait(m);
    }
    pub fn multi_check(self: *Threading, node_buffer: []*Queue.Node, pollfd_buffer: []POLLFD) void {
        const m = &self.main_queue.mutex;
        var size: u8 = 0;
        m.lock();
        if (self.main_queue.queue.len == 0) self.main_queue.condition.wait(m);
        while (self.main_queue.queue.popFirst()) |node| : (size += 1) {
            // std.debug.print("run\n", .{});
            node_buffer[size] = node;
            pollfd_buffer[size] = .{
                .fd = node.data.stream.handle,
                .events = POLLIN,
                .revents = 0,
            };
            if (size == node_buffer.len - 1) break;
        }
        m.unlock();
        _ = poll(pollfd_buffer.ptr, size, 0);
        for (pollfd_buffer[0..size], 0..) |pfd, i| {
            if (pfd.revents & POLLIN != 0) {
                HttpServer.mainloop_noerr(self.server, node_buffer[i].data);
                self.main_queue.alloc.destroy(node_buffer[i]);
            } else {
                self.main_queue.push_node(node_buffer[i]);
            }
        }
    }
};

pub fn create(server: *HttpServer, alloc: std.mem.Allocator, threads: u64) Threading {
    return .{
        .threads = alloc.alloc(std.Thread, threads) catch unreachable,
        .server = server,
        .main_queue = .{
            .alloc = alloc,
        },
    };
}
pub fn deinit(self: *Threading) void {
    for (self.threads) |*thread| {
        thread.join();
    }
    self.main_queue.alloc.free(self.threads);
    while (self.main_queue.queue.popFirst()) |node| {
        node.data.stream.close();
        self.main_queue.alloc.destroy(node);
    }
}

pub fn data_waiting(client: StreamServer.Connection) !bool {
    var pollfd: [1]POLLFD = .{.{
        .fd = client.stream.handle,
        .events = POLLIN,
        .revents = undefined,
    }};
    _ = poll(&pollfd, 1, 0);
    if (pollfd[0].revents & POLLIN != 0) {
        return true;
    }
    return false;
}

pub fn start_threads(self: *Threading) !void {
    if (self.threads.len == 0) return;
    for (self.threads) |*thread| {
        thread.* = try std.Thread.spawn(.{}, Work.work, .{self});
    }
}
