pub const RequestBuilder = struct {
    response: []const u8,

    pub fn init11(comptime method: []const u8, comptime path: []const u8) @This() {
        return .{ .response = method ++ " /" ++ path ++ " " ++ "HTTP/1.1" ++ "\r\n" };
    }
    pub fn init(comptime method: []const u8, comptime path: []const u8, comptime version: []const u8) @This() {
        return .{ .response = method ++ " " ++ path ++ " " ++ version ++ "\r\n" };
    }
    // pub fn add_code(comptime version: []const u8, comptime code: []const u8) @This() {
    //     return .{ .response = version ++ " " ++ code ++ " " ++ "\r\n" };
    // }
    pub fn content_type(comptime self: @This(), comptime value: []const u8) @This() {
        return .{ .response = self.response ++ "Content-Type: " ++ value ++ "\r\n" };
    }
    pub fn connection(comptime self: @This(), comptime value: enum { @"keep-alive", close }) @This() {
        return .{ .response = self.response ++ "Connection: " ++ @tagName(value) ++ "\r\n" };
    }
    pub fn add_field(comptime self: @This(), comptime name: []const u8, comptime value: []const u8) @This() {
        return .{ .response = self.response ++ name ++ ": " ++ value ++ "\r\n" };
    }
    pub fn content_length(comptime self: @This(), comptime value: []const u8) @This() {
        return .{ .response = self.response ++ "Content-Length: " ++ value ++ "\r\n" };
    }
    pub fn location(comptime self: @This(), comptime value: []const u8) @This() {
        return .{ .response = self.response ++ "Location: " ++ value ++ "\r\n" };
    }
    pub fn end(comptime self: @This()) *const [self.response.len + 2]u8 {
        return @as(*const [self.response.len + 2]u8, self.response ++ "\r\n");
    }
};
const std = @import("std");
const HttpServer = @import("HttpServer.zig");

test "404 File Not File" {
    var server: HttpServer = undefined;
    var arg: @import("args.zig").HTTP_CONFIG = undefined;
    try arg.init(std.testing.allocator);
    arg.gen_port = true;
    arg.threads = 1;
    server.init(std.testing.allocator, arg);
    var thread = try std.Thread.spawn(.{}, struct {
        fn a(s: *HttpServer) void {
            s.start();
        }
    }.a);
    defer thread.join();
    var client = std.http.Client{};
    _ = client;
}
